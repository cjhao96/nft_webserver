// log
import store from "../store";

const fetchDataRequest = () => {
    return {
        type: "CHECK_DATA_REQUEST",
    };
};

const fetchDataSuccess = (payload) => {
    return {
        type: "CHECK_DATA_SUCCESS",
        payload: payload,
    };
};

const fetchDataFailed = (payload) => {
    return {
        type: "CHECK_DATA_FAILED",
        payload: payload,
    };
};

export const fetchData = (account) => {
    return async (dispatch) => {
        dispatch(fetchDataRequest());
        try {
            // const CEXTRACT_ADMIN_ROLE = await store.getState().blockchain.cExtractERC721A.methods.DEFAULT_ADMIN_ROLE().call();
            // const CEXTRACT_MINTER_ROLE = await store.getState().blockchain.cExtractERC721A.methods.MINTER_ROLE().call();
            // const CEXTRACT_WHITELIST_ROLE = await store.getState().blockchain.cExtractERC721A.methods.WHITELIST_ROLE().call();

            // const CEXTRACT_isAdmin = await store.getState().blockchain.cExtractERC721A.methods.hasRole(CEXTRACT_ADMIN_ROLE, account).call();
            // const CEXTRACT_isMinter = await store.getState().blockchain.cExtractERC721A.methods.hasRole(CEXTRACT_MINTER_ROLE, account).call();
            // const CEXTRACT_isWhitelister = await store.getState().blockchain.cExtractERC721A.methods.hasRole(CEXTRACT_WHITELIST_ROLE, account).call();
            
            // const CEXTRACT_mintIsActive = await store.getState().blockchain.cExtractERC721A.methods.mintIsActive().call();
            // const CEXTRACT_whitelistActive = await store.getState().blockchain.cExtractERC721A.methods.whitelistActive().call();

            // const CEXTRACT_maxSupply = await store.getState().blockchain.cExtractERC721A.methods.maxSupply().call();
            // const CEXTRACT_maxMintAmountPerTrx = await store.getState().blockchain.cExtractERC721A.methods.maxMintAmountPerTrx().call();
            // const CEXTRACT_maxAmountPerWallet = await store.getState().blockchain.cExtractERC721A.methods.maxAmountPerWallet().call();

            // const CEXTRACT_allCExtractsOwned = await store.getState().blockchain.cExtractERC721A.methods.getAllTokensOwned().call({
            //     from: account
            // });

            const ERC721A_MERKLETREE_owner = await store.getState().blockchain.erc721A_MerkleTree.methods.owner().call();

            const ERC721A_MERKLETREE_mintCost = await store.getState().blockchain.erc721A_MerkleTree.methods.mintCost().call();
            const ERC721A_MERKLETREE_nameChangeCost = await store.getState().blockchain.erc721A_MerkleTree.methods.nameChangeCost().call();

            const ERC721A_MERKLETREE_maxSupply = await store.getState().blockchain.erc721A_MerkleTree.methods.maxSupply().call();
            const ERC721A_MERKLETREE_maxMintAmountPerTrx = await store.getState().blockchain.erc721A_MerkleTree.methods.maxMintAmountPerTrx().call();
            const ERC721A_MERKLETREE_maxAmountPerWallet = await store.getState().blockchain.erc721A_MerkleTree.methods.maxAmountPerWallet().call();

            const ERC721A_MERKLETREE_allowToMint = await store.getState().blockchain.erc721A_MerkleTree.methods.allowToMint().call();
            const ERC721A_MERKLETREE_freeToMint = await store.getState().blockchain.erc721A_MerkleTree.methods.freeToMint().call();
            const ERC721A_MERKLETREE_whitelistedOnly = await store.getState().blockchain.erc721A_MerkleTree.methods.whitelistedOnly().call();

            const ERC721A_MERKLETREE_allERC721AOwned = [];
            
            const ERC721A_MERKLETREE_allERC721AOwnedIndex = await store.getState().blockchain.erc721A_MerkleTree.methods.getAllTokensOwned().call({
                from: account
            });
            
            for (let i = 0; i < ERC721A_MERKLETREE_allERC721AOwnedIndex.length; i++) {
                const index = ERC721A_MERKLETREE_allERC721AOwnedIndex[i];
                const name = await store.getState().blockchain.erc721A_MerkleTree.methods.getTokenName(ERC721A_MERKLETREE_allERC721AOwnedIndex[i]).call();
                
                const itemName = (name == "") ? `item_${index}` : name;

                ERC721A_MERKLETREE_allERC721AOwned[i] = { index, itemName };
            }

            const REWARDS_ADMIN_ROLE = await store.getState().blockchain.rewardsToken.methods.DEFAULT_ADMIN_ROLE().call();
            const REWARDS_MINTER_ROLE = await store.getState().blockchain.rewardsToken.methods.MINTER_ROLE().call();
            const REWARDS_PAUSER_ROLE = await store.getState().blockchain.rewardsToken.methods.PAUSER_ROLE().call();

            const REWARDS_isAdmin = await store.getState().blockchain.rewardsToken.methods.hasRole(REWARDS_ADMIN_ROLE, account).call();
            const REWARDS_isMinter = await store.getState().blockchain.rewardsToken.methods.hasRole(REWARDS_MINTER_ROLE, account).call();
            const REWARDS_isPauser = await store.getState().blockchain.rewardsToken.methods.hasRole(REWARDS_PAUSER_ROLE, account).call();

            const REWARDS_paused = await store.getState().blockchain.rewardsToken.methods.paused().call();
            const REWARDS_totalSupply = await store.getState().blockchain.rewardsToken.methods.totalSupply().call();
            const REWARDS_balance = await store.getState().blockchain.rewardsToken.methods.balanceOf(account).call();
            

            const STAKES_ADMIN_ROLE = await store.getState().blockchain.stakesToken.methods.DEFAULT_ADMIN_ROLE().call();
            const STAKES_MINTER_ROLE = await store.getState().blockchain.stakesToken.methods.MINTER_ROLE().call();
            const STAKES_PAUSER_ROLE = await store.getState().blockchain.stakesToken.methods.PAUSER_ROLE().call();

            const STAKES_isAdmin = await store.getState().blockchain.stakesToken.methods.hasRole(STAKES_ADMIN_ROLE, account).call();
            const STAKES_isMinter = await store.getState().blockchain.stakesToken.methods.hasRole(STAKES_MINTER_ROLE, account).call();
            const STAKES_isPauser = await store.getState().blockchain.stakesToken.methods.hasRole(STAKES_PAUSER_ROLE, account).call();

            const STAKES_paused = await store.getState().blockchain.stakesToken.methods.paused().call();
            const STAKES_totalSupply = await store.getState().blockchain.stakesToken.methods.totalSupply().call();
            const STAKES_balance = await store.getState().blockchain.stakesToken.methods.balanceOf(account).call();

            
            const ERC20Staking_balances = await store.getState().blockchain.erc20Staking.methods.balances(account).call();

            const ERC721Staking_balances = await store.getState().blockchain.erc721Staking.methods.balances(account).call();


            // let owner = await store
            //     .getState()
            //     .blockchain.cExtractToken.methods.owner()
            //     .call();

            // let allOwnerCExtracts = await store
            //     .getState()
            //     .blockchain.cExtractToken.methods.getAllTokensOwned()
            //     .call({
            //         from: account
            //     });

            // let baseURI = await store
            //     .getState()
            //     .blockchain.cExtractToken.methods.baseURI()
            //     .call();

            // let balance = await store
            //     .getState()
            //     .blockchain.cExtractToken.methods.balanceOf(account)
            //     .call();
            
            dispatch(
                fetchDataSuccess({
                    // cExtractERC721AState: {
                    //     isAdmin: CEXTRACT_isAdmin,
                    //     isMinter: CEXTRACT_isMinter,
                    //     isWhitelister: CEXTRACT_isWhitelister,
                    //     mintIsActive: CEXTRACT_mintIsActive,
                    //     whitelistActive: CEXTRACT_whitelistActive,
                    //     maxSupply: CEXTRACT_maxSupply,
                    //     maxMintAmountPerTrx: CEXTRACT_maxMintAmountPerTrx,
                    //     maxAmountPerWallet: CEXTRACT_maxAmountPerWallet,
                    //     allCExtractsOwned: CEXTRACT_allCExtractsOwned,
                    // },
                    erc721A_MerkleTreeState: {
                        isOwner: (ERC721A_MERKLETREE_owner == account),
                        mintCost: ERC721A_MERKLETREE_mintCost,
                        nameChangeCost: ERC721A_MERKLETREE_nameChangeCost,
                        maxSupply: ERC721A_MERKLETREE_maxSupply,
                        maxMintAmountPerTrx: ERC721A_MERKLETREE_maxMintAmountPerTrx,
                        maxAmountPerWallet: ERC721A_MERKLETREE_maxAmountPerWallet,
                        allowToMint: ERC721A_MERKLETREE_allowToMint,
                        freeToMint: ERC721A_MERKLETREE_freeToMint,
                        whitelistedOnly: ERC721A_MERKLETREE_whitelistedOnly,
                        allERC721AOwned: ERC721A_MERKLETREE_allERC721AOwned
                    },
                    rewardsTokenState: {
                        isAdmin: REWARDS_isAdmin,
                        isMinter: REWARDS_isMinter,
                        isPauser: REWARDS_isPauser,
                        paused: REWARDS_paused,
                        totalSupply: REWARDS_totalSupply,
                        balance: REWARDS_balance,
                    },
                    stakesTokenState: {
                        isAdmin: STAKES_isAdmin,
                        isMinter: STAKES_isMinter,
                        isPauser: STAKES_isPauser,
                        paused: STAKES_paused,
                        totalSupply: STAKES_totalSupply,
                        balance: STAKES_balance,
                    },
                    erc20StakingState: {
                        balances: ERC20Staking_balances,
                    },
                    erc721StakingState: {
                        balances: ERC721Staking_balances,
                    },
                    // owner: owner.toLowerCase(),
                    // allOwnerCExtracts,
                    // baseURI,
                    // balance,
                })
            );
        } catch (err) {
            console.log(err);
            dispatch(fetchDataFailed("Could not load data from contract."));
        }
    };
};