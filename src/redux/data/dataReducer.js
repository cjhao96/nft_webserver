const initialState = {
    loading: false,
    // cExtractERC721AState: {
    //     isAdmin: false,
    //     isMinter: false,
    //     isWhitelister: false,
    //     mintIsActive: false,
    //     whitelistActive: false,
    //     maxSupply: 0,
    //     maxMintAmountPerTrx: 0,
    //     maxAmountPerWallet: 0,
    //     allCExtractsOwned: []
    // },
    erc721A_MerkleTreeState: {
        isOwner: false,
        mintCost: 0,
        nameChangeCost: 0,
        maxSupply: 0,
        maxMintAmountPerTrx: 0,
        maxAmountPerWallet: 0,
        allowToMint: false,
        freeToMint: false,
        whitelistedOnly: false,
        allERC721AOwned: []
    },
    rewardsTokenState: {
        isAdmin: false,
        isMinter: false,
        isPauser: false,
        paused: false,
        totalSupply: 0,
        balance: 0,
    },
    stakesTokenState: {
        isAdmin: false,
        isMinter: false,
        isPauser: false,
        paused: false,
        totalSupply: 0,
        balance: 0,
    },
    erc20StakingState: {
        balances: 0,
    },
    erc721StakingState: {
        balances: 0,
    },
    // owner: null,
    // allOwnerCExtracts: [],
    // baseURI: "",
    // balance: 0,
    error: false,
    errorMsg: "",
};

const dataReducer = (state = initialState, action) => {
    switch (action.type) {
        case "CHECK_DATA_REQUEST":
            return {
                ...initialState,
                loading: true,
            };
        case "CHECK_DATA_SUCCESS":
            return {
                ...initialState,
                loading: false,
                // cExtractERC721AState: {
                //     isAdmin: action.payload.cExtractERC721AState.isAdmin,
                //     isMinter: action.payload.cExtractERC721AState.isMinter,
                //     isWhitelister: action.payload.cExtractERC721AState.isWhitelister,
                //     mintIsActive: action.payload.cExtractERC721AState.mintIsActive,
                //     whitelistActive: action.payload.cExtractERC721AState.whitelistActive,
                //     maxSupply: action.payload.cExtractERC721AState.maxSupply,
                //     maxMintAmountPerTrx: action.payload.cExtractERC721AState.maxMintAmountPerTrx,
                //     maxAmountPerWallet: action.payload.cExtractERC721AState.maxAmountPerWallet,
                //     allCExtractsOwned: action.payload.cExtractERC721AState.allCExtractsOwned,
                // },
                erc721A_MerkleTreeState: {
                    isOwner: action.payload.erc721A_MerkleTreeState.isOwner,
                    mintCost: action.payload.erc721A_MerkleTreeState.mintCost,
                    nameChangeCost: action.payload.erc721A_MerkleTreeState.nameChangeCost,
                    maxSupply: action.payload.erc721A_MerkleTreeState.maxSupply,
                    maxMintAmountPerTrx: action.payload.erc721A_MerkleTreeState.maxMintAmountPerTrx,
                    maxAmountPerWallet: action.payload.erc721A_MerkleTreeState.maxAmountPerWallet,
                    allowToMint: action.payload.erc721A_MerkleTreeState.allowToMint,
                    freeToMint: action.payload.erc721A_MerkleTreeState.freeToMint,
                    whitelistedOnly: action.payload.erc721A_MerkleTreeState.whitelistedOnly,
                    allERC721AOwned: action.payload.erc721A_MerkleTreeState.allERC721AOwned
                },
                rewardsTokenState: {
                    isAdmin: action.payload.rewardsTokenState.isAdmin,
                    isMinter: action.payload.rewardsTokenState.isMinter,
                    isPauser: action.payload.rewardsTokenState.isPauser,
                    paused: action.payload.rewardsTokenState.paused,
                    totalSupply: action.payload.rewardsTokenState.totalSupply,
                    balance: action.payload.rewardsTokenState.balance,
                },
                stakesTokenState: {
                    isAdmin: action.payload.stakesTokenState.isAdmin,
                    isMinter: action.payload.stakesTokenState.isMinter,
                    isPauser: action.payload.stakesTokenState.isPauser,
                    paused: action.payload.stakesTokenState.paused,
                    totalSupply: action.payload.stakesTokenState.totalSupply,
                    balance: action.payload.stakesTokenState.balance,
                },
                erc20StakingState: {
                    balances: action.payload.erc20StakingState.balances,
                },
                erc721StakingState: {
                    balances: action.payload.erc721StakingState.balances,
                },
                // owner: action.payload.owner,
                // allOwnerCExtracts: action.payload.allOwnerCExtracts,
                // baseURI: action.payload.baseURI,
                // balance: action.payload.balance,
            };
        case "CHECK_DATA_FAILED":
            return {
                ...initialState,
                loading: false,
                error: true,
                errorMsg: action.payload,
            };
        default:
            return state;
    }
};

export default dataReducer;