const initialState = {
    loading: false,
    account: null,
    // cExtractToken: null,
    // cExtractERC721A: null,
    erc721A_MerkleTree: null,
    erc20Staking: null,
    erc721Staking: null,
    rewardsToken: null,
    stakesToken: null,
    web3: null,
    errorMsg: "",
};

const blockchainReducer = (state = initialState, action) => {
    switch (action.type) {
        case "CONNECTION_REQUEST":
            return {
                ...initialState,
                loading: true,
            };
        case "CONNECTION_SUCCESS":
            return {
                ...state,
                loading: false,
                account: action.payload.account,
                // cExtractToken: action.payload.cExtractToken,
                // cExtractERC721A: action.payload.cExtractERC721A,
                erc721A_MerkleTree: action.payload.erc721A_MerkleTree,
                erc20Staking: action.payload.erc20Staking,
                erc721Staking: action.payload.erc721Staking,
                rewardsToken: action.payload.rewardsToken,
                stakesToken: action.payload.stakesToken,
                web3: action.payload.web3,
            };
        case "CONNECTION_FAILED":
            return {
                ...initialState,
                loading: false,
                errorMsg: action.payload,
            };
        case "UPDATE_ACCOUNT":
            return {
                ...state,
                account: action.payload.account,
            };
        default:
            return state;
    }
};

export default blockchainReducer;