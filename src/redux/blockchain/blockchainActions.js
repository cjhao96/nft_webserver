// constants
import Web3 from "web3";
// import CExtractToken from "../../artifacts/CExtractToken/CExtractToken.json";
// import CExtractERC721A from "../../artifacts/CExtractERC721A/CExtractERC721A.json";
import ERC721A_MerkleTree from "../../artifacts/ERC721A_MerkleTree/ERC721A_MerkleTree.json";
import ERC20Staking from "../../artifacts/ERC20Staking/ERC20Staking.json";
import ERC721Staking from "../../artifacts/ERC721Staking/ERC721Staking.json";
import RewardsToken from "../../artifacts/RewardsToken/RewardsToken.json";
import StakesToken from "../../artifacts/StakesToken/StakesToken.json";
import config from "../../config/default";

// log
import { fetchData } from "../data/dataActions";

// Network ID
const NETWORK_ID = config.networkId;//1649987944060;//1649932712121;1650876758098

// Contract Address
// const CEXTRACT_TOKEN_ADDRESS = "0x47d132726F897A0a4565aB475B2F305f3a5f3F76";
// const CEXTRACT_ERC721A_ADDRESS = "0x04287b3767BB2DE9427ED26e59EE859e08A872E7";
const ERC721A_MERKLETREE_ADDRESS = config.erc721AMerkleTree;//"0x70DDDD0fA72594fa5B6Bc34daA5d646d8957C6e7";
const ERC20_STAKING_ADDRESS = config.erc20Staking;//"0xdB1FdfcC2924A8774FE05214BDb82Ac16Fed7696";
const ERC721_STAKING_ADDRESS = config.erc721Staking;//"0xd6bfC4e61dF87D5392a590A0134AA03063ee9295";
const REWARDS_TOKEN_ADDRESS = config.rewardsToken;//"0x916Df63521245bd1011307c5b1ba611285f2d98B";
const STAKES_TOKEN_ADDRESS = config.stakesToken;//"0x7F629E381472079F845F4cb0F5Cd737Ad8e62E8B";

const connectRequest = () => {
    return {
        type: "CONNECTION_REQUEST",
    };
};

const connectSuccess = (payload) => {
    return {
        type: "CONNECTION_SUCCESS",
        payload: payload,
    };
};

const connectFailed = (payload) => {
    return {
        type: "CONNECTION_FAILED",
        payload: payload,
    };
};

const updateAccountRequest = (payload) => {
    return {
        type: "UPDATE_ACCOUNT",
        payload: payload,
    };
};

export const connect = () => {
    return async (dispatch) => {
        dispatch(connectRequest());
        if (window.ethereum) {
            let web3 = new Web3(window.ethereum);
            try {
                const accounts = await window.ethereum.request({ method: "eth_requestAccounts" });
                // console.log(accounts);

                const networkId = await window.ethereum.request({ method: "net_version" });
                console.log(networkId);
                
                if (networkId == NETWORK_ID) {
                    // const cExtractToken = new web3.eth.Contract(
                    //     CExtractToken.abi,
                    //     CEXTRACT_TOKEN_ADDRESS,
                    // );
                    
                    // const cExtractERC721A = new web3.eth.Contract(
                    //     CExtractERC721A.abi,
                    //     CEXTRACT_ERC721A_ADDRESS,
                    // );

                    const erc721A_MerkleTree = new web3.eth.Contract(
                        ERC721A_MerkleTree.abi,
                        ERC721A_MERKLETREE_ADDRESS,
                    );
                        
                    const erc20Staking = new web3.eth.Contract(
                        ERC20Staking.abi,
                        ERC20_STAKING_ADDRESS,
                    );

                    const erc721Staking = new web3.eth.Contract(
                        ERC721Staking.abi,
                        ERC721_STAKING_ADDRESS,
                    );

                    const rewardsToken = new web3.eth.Contract(
                        RewardsToken.abi,
                        REWARDS_TOKEN_ADDRESS,
                    );

                    const stakesToken = new web3.eth.Contract(
                        StakesToken.abi,
                        STAKES_TOKEN_ADDRESS,
                    );

                    dispatch(
                        connectSuccess({
                            account: accounts[0],
                            // cExtractToken: cExtractToken,
                            // cExtractERC721A: cExtractERC721A,
                            erc721A_MerkleTree: erc721A_MerkleTree,
                            erc20Staking: erc20Staking,
                            erc721Staking: erc721Staking,
                            rewardsToken: rewardsToken,
                            stakesToken: stakesToken,
                            web3: web3,
                        })
                    );
                    // Add listeners start
                    window.ethereum.on("accountsChanged", (accounts) => {
                        dispatch(updateAccount(accounts[0]));
                    });
                    window.ethereum.on("chainChanged", () => {
                        window.location.reload();
                    });
                    // Add listeners end
                } else {
                    dispatch(connectFailed("Change network to localhost."));
                }
            } catch (err) {
                console.log(err);
                dispatch(connectFailed("Something went wrong."));
            }
        } else {
            dispatch(connectFailed("Install Metamask."));
        }
    };
};

export const updateAccount = (account) => {
    return async (dispatch) => {
        dispatch(updateAccountRequest({ account: account }));
        dispatch(fetchData(account));
    };
};