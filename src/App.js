import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { connect } from "./redux/blockchain/blockchainActions";
import { fetchData } from "./redux/data/dataActions";
import './App.css';
import Renderer from "./components/renderer";
import * as gs from "./styles/globalStyles";
import config from "./config/default";
import { MerkleTree } from "merkletreejs";
import keccak256 from "keccak256";

const buf2hex = x => "0x" + x.toString('hex');
const leaves = config.addresses.map(x => keccak256(x));
const tree = new MerkleTree(leaves, keccak256, { sortPairs: true });

function App() {
    const dispatch = useDispatch();
    const blockchain = useSelector((state) => state.blockchain);
    const data = useSelector((state) => state.data);
    
    const [loading, setLoading] = useState(false);

    // CExtract ERC721A
    // const [newMaxSupply_cExtract, setNewMaxSupply_cExtract] = useState(0);
    // const [newMaxTrx_cExtract, setNewMaxTrx_cExtract] = useState(0);
    // const [newMaxWallet_cExtract, setNewMaxWallet_cExtract] = useState(0);

    // const [mintAmount_cExtract, setMintAmount_cExtract] = useState(1);
    // const [whiteListMintAmount_cExtract, setWhiteListMintAmount_cExtract] = useState(1);

    // const [grantMinterAddress_cExtract, setGrantMinterAddress_cExtract] = useState("");
    // const [grantWhitelistAddress_cExtract, setGrantWhitelistAddress_cExtract] = useState("");

    // const [revokeMinterAddress_cExtract, setRevokeMinterAddress_cExtract] = useState("");
    // const [revokeWhitelistAddress_cExtract, setRevokeWhitelistAddress_cExtract] = useState("");

    // const [balance_cExtract, setBalance_cExtract] = useState(0);
    // const [balanceOfAddress_cExtract, setBalanceOfAddress_cExtract] = useState("");

    // const [ownerOfAddress_cExtract, setOwnerOfAddress_cExtract] = useState("0x00...");
    // const [ownerOfTokenId_cExtract, setOwnerOfTokenId_cExtract] = useState(0);

    // ERC721A_MerkleTree
    const [newURI_erc721AMerkleTree, setNewURI_erc721AMerkleTree] = useState("");
    const [newMintCost_erc721AMerkleTree, setNewMintCost_erc721AMerkleTree] = useState(0);
    const [newNameChangeCost_erc721AMerkleTree, setNewNameChangeCost_erc721AMerkleTree] = useState(0);
    const [newMaxSupply_erc721AMerkleTree, setNewMaxSupply_erc721AMerkleTree] = useState(0);
    const [newMaxMintAmountPerTrx_erc721AMerkleTree, setNewMaxMintAmountPerTrx_erc721AMerkleTree] = useState(0);
    const [newMaxAmountPerWallet_erc721AMerkleTree, setNewMaxAmountPerWallet_erc721AMerkleTree] = useState(0);
    const [root_erc721AMerkleTree, setRoot_erc721AMerkleTree] = useState("");
    const [tokenId_erc721AMerkleTree, setTokenId_erc721AMerkleTree] = useState("");
    const [newName_erc721AMerkleTree, setNewName_erc721AMerkleTree] = useState("");
    const [mintAmount_erc721AMerkleTree, setMintAmount_erc721AMerkleTree] = useState(0);
    
    const [checkName_erc721AMerkleTree, setCheckName_erc721AMerkleTree] = useState("");
    const [isNameReserved_erc721AMerkleTree, setIsNameReserved_erc721AMerkleTree] = useState(false);
    
    const [checkTokenId_erc721AMerkleTree, setCheckTokenId_erc721AMerkleTree] = useState("");
    const [tokenName_erc721AMerkleTree, setTokenName_erc721AMerkleTree] = useState("");
    
    const [proof_erc721AMerkleTree, setProof_erc721AMerkleTree] = useState("");
    const [isValid_erc721AMerkleTree, setIsValid_erc721AMerkleTree] = useState(false);

    const [balanceOfAddress_erc721AMerkleTree, setBalanceOfAddress_erc721AMerkleTree] = useState("");
    const [balance_erc721AMerkleTree, setBalance_erc721AMerkleTree] = useState(0);

    const [ownerOfTokenId_erc721AMerkleTree, setOwnerOfTokenId_erc721AMerkleTree] = useState("");
    const [ownerAddress_erc721AMerkleTree, setOwnerAddress_erc721AMerkleTree] = useState("");

    // Rewards Token
    const [mintTo_rewards, setMintTo_rewards] = useState("");
    const [mintAmount_rewards, setMintAmount_rewards] = useState(1);

    const [balance_rewards, setBalance_rewards] = useState(0);
    const [balanceOfAddress_rewards, setBalanceOfAddress_rewards] = useState("0x00...");
    
    // Stakes Token
    const [mintTo_stakes, setMintTo_stakes] = useState("");
    const [mintAmount_stakes, setMintAmount_stakes] = useState(1);

    const [balance_stakes, setBalance_stakes] = useState(0);
    const [balanceOfAddress_stakes, setBalanceOfAddress_stakes] = useState("0x00...");

    // ERC20 Staking
    const [stakeAmount_erc20Staking, setStakeAmount_erc20Staking] = useState(1);
    const [withdrawAmount_erc20Staking, setWithdrawAmount_erc20Staking] = useState(1);

    const [balances_erc20Staking, setBalances_erc20Staking] = useState(0);
    const [balancesAddress_erc20Staking, setBalancesAddress_erc20Staking] = useState("0x00...");

    const [earned_erc20Staking, setEarned_erc20Staking] = useState(0);
    const [earnedAddress_erc20Staking, setEarnedAddress_erc20Staking] = useState("0x00...");

    // ERC721 Staking
    const [stakeId_erc721Staking, setStakeId_erc721Staking] = useState("");
    const [withdrawId_erc721Staking, setWithdrawId_erc721Staking] = useState("");

    const [balances_erc721Staking, setBalances_erc721Staking] = useState(0);
    const [balancesAddress_erc721Staking, setBalancesAddress_erc721Staking] = useState("0x00...");

    const [earned_erc721Staking, setEarned_erc721Staking] = useState(0);
    const [earnedAddress_erc721Staking, setEarnedAddress_erc721Staking] = useState("0x00...");


    // const CExtractERC721AMethods = {
    //     toggleMintStatus: () => {
    //         setLoading(true);
    //         blockchain.cExtractERC721A.methods.toggleMintStatus().send({
    //             from: blockchain.account
    //         }).once("error", (err) => {
    //             setLoading(false);
    //             console.log(err);
    //         }).then((receipt) => {
    //             setLoading(false);
    //             console.log(receipt);
    //             dispatch(fetchData(blockchain.account));
    //         });
    //     },
    //     toggleWhitelistStatus: () => {
    //         setLoading(true);
    //         blockchain.cExtractERC721A.methods.toggleWhitelistStatus().send({
    //             from: blockchain.account
    //         }).once("error", (err) => {
    //             setLoading(false);
    //             console.log(err);
    //         }).then((receipt) => {
    //             setLoading(false);
    //             console.log(receipt);
    //             dispatch(fetchData(blockchain.account));
    //         });
    //     },
    //     setMaxSupply: (_amount) => {
    //         setLoading(true);
    //         blockchain.cExtractERC721A.methods.setMaxSupply(_amount).send({
    //             from: blockchain.account
    //         }).on("error", (error) => {
    //             setLoading(false);
    //             console.log(error);
    //         }).then((receipt) => {
    //             setLoading(false);
    //             console.log(receipt);
    //             dispatch(fetchData(blockchain.account));
    //         });
    //     },
    //     setMaxMintAmountPerTrx: (_amount) => {
    //         setLoading(true);
    //         blockchain.cExtractERC721A.methods.setMaxMintAmountPerTrx(_amount).send({
    //             from: blockchain.account
    //         }).on("error", (error) => {
    //             setLoading(false);
    //             console.log(error);
    //         }).then((receipt) => {
    //             setLoading(false);
    //             console.log(receipt);
    //             dispatch(fetchData(blockchain.account));
    //         });
    //     },
    //     setMaxAmountPerWallet: (_amount) => {
    //         setLoading(true);
    //         blockchain.cExtractERC721A.methods.setMaxAmountPerWallet(_amount).send({
    //             from: blockchain.account
    //         }).on("error", (error) => {
    //             setLoading(false);
    //             console.log(error);
    //         }).then((receipt) => {
    //             setLoading(false);
    //             console.log(receipt);
    //             dispatch(fetchData(blockchain.account));
    //         });
    //     },
    //     grantMinter: (address) => {
    //         setLoading(true);
    //         blockchain.cExtractERC721A.methods.grantMinter(address).send({
    //             from: blockchain.account
    //         }).on("error", (error) => {
    //             setLoading(false);
    //             console.log(error);
    //         }).then((result) => {
    //             setLoading(false);
    //             console.log(result);
    //             dispatch(fetchData(blockchain.account));
    //         });
    //     },
    //     removeMinter: (address) => {
    //         setLoading(true);
    //         blockchain.cExtractERC721A.methods.removeMinter(address).send({
    //             from: blockchain.account
    //         }).on("error", (error) => {
    //             setLoading(false);
    //             console.log(error);
    //         }).then((result) => {
    //             setLoading(false);
    //             console.log(result);
    //             dispatch(fetchData(blockchain.account));
    //         });
    //     },
    //     addWhitelistAccount: (address) => {
    //         setLoading(true);
    //         blockchain.cExtractERC721A.methods.addWhitelistAccount(address).send({
    //             from: blockchain.account
    //         }).on("error", (error) => {
    //             setLoading(false);
    //             console.log(error);
    //         }).then((result) => {
    //             setLoading(false);
    //             console.log(result);
    //             dispatch(fetchData(blockchain.account));
    //         });
    //     },
    //     removeWhitelistAccount: (address) => {
    //         setLoading(true);
    //         blockchain.cExtractERC721A.methods.removeWhitelistAccount(address).send({
    //             from: blockchain.account
    //         }).on("error", (error) => {
    //             setLoading(false);
    //             console.log(error);
    //         }).then((result) => {
    //             setLoading(false);
    //             console.log(result);
    //             dispatch(fetchData(blockchain.account));
    //         });
    //     },
    //     mint: (_amount) => {
    //         setLoading(true);
    //         blockchain.cExtractERC721A.methods.mint(_amount).send({
    //             from: blockchain.account
    //         }).on("error", (error) => {
    //             setLoading(false);
    //             console.log(error);
    //         }).then((receipt) => {
    //             setLoading(false);
    //             console.log(receipt);
    //             dispatch(fetchData(blockchain.account));
    //         });
    //     },
    //     whitelistMint: (_amount) => {
    //         setLoading(true);
    //         blockchain.cExtractERC721A.methods.whitelistMint(_amount).send({
    //             from: blockchain.account
    //         }).on("error", (error) => {
    //             setLoading(false);
    //             console.log(error);
    //         }).then((receipt) => {
    //             setLoading(false);
    //             console.log(receipt);
    //             dispatch(fetchData(blockchain.account));
    //         });
    //     },
    //     balanceOf: (_address) => {
    //         setLoading(true);
    //         blockchain.cExtractERC721A.methods.balanceOf(_address).call({
    //             from: blockchain.account
    //         }).then((result) => {
    //             setLoading(false);
    //             console.log(result);
    //             setBalance_cExtract(result);
    //             dispatch(fetchData(blockchain.account));
    //         });
    //     },
    //     ownerOf: (_tokenId) => {
    //         setLoading(true);
    //         blockchain.cExtractERC721A.methods.ownerOf(_tokenId).call({
    //             from: blockchain.account
    //         }).then((result) => {
    //             setLoading(false);
    //             console.log(result);
    //             setOwnerOfAddress_cExtract(result);
    //             dispatch(fetchData(blockchain.account));
    //         });
    //     }
    // };

    const ERC721A_MerkleTreeMethods = {
        updateBaseURI: (_uri) => {
            setLoading(true);
            blockchain.erc721A_MerkleTree.methods.updateBaseURI(_uri).send({
                from: blockchain.account
            }).once("error", (err) => {
                setLoading(false);
                console.log(err);
            }).then((receipt) => {
                setLoading(false);
                console.log(receipt);
                dispatch(fetchData(blockchain.account));
            });
        },
        setMintCost: (_mintCost) => {
            setLoading(true);
            blockchain.erc721A_MerkleTree.methods.setMintCost(_mintCost).send({
                from: blockchain.account
            }).once("error", (err) => {
                setLoading(false);
                console.log(err);
            }).then((receipt) => {
                setLoading(false);
                console.log(receipt);
                dispatch(fetchData(blockchain.account));
            });
        },
        setNameChangeCost: (_nameChangeCost) => {
            setLoading(true);
            blockchain.erc721A_MerkleTree.methods.setNameChangeCost(_nameChangeCost).send({
                from: blockchain.account
            }).once("error", (err) => {
                setLoading(false);
                console.log(err);
            }).then((receipt) => {
                setLoading(false);
                console.log(receipt);
                dispatch(fetchData(blockchain.account));
            });
        },
        setMaxSupply: (_newMaxSupply) => {
            setLoading(true);
            blockchain.erc721A_MerkleTree.methods.setMaxSupply(_newMaxSupply).send({
                from: blockchain.account
            }).on("error", (error) => {
                setLoading(false);
                console.log(error);
            }).then((receipt) => {
                setLoading(false);
                console.log(receipt);
                dispatch(fetchData(blockchain.account));
            });
        },
        setMaxMintAmountPerTrx: (_maxMintAmountPerTrx) => {
            setLoading(true);
            blockchain.erc721A_MerkleTree.methods.setMaxMintAmountPerTrx(_maxMintAmountPerTrx).send({
                from: blockchain.account
            }).on("error", (error) => {
                setLoading(false);
                console.log(error);
            }).then((receipt) => {
                setLoading(false);
                console.log(receipt);
                dispatch(fetchData(blockchain.account));
            });
        },
        setMaxAmountPerWallet: (_maxAmountPerWallet) => {
            setLoading(true);
            blockchain.erc721A_MerkleTree.methods.setMaxAmountPerWallet(_maxAmountPerWallet).send({
                from: blockchain.account
            }).on("error", (error) => {
                setLoading(false);
                console.log(error);
            }).then((receipt) => {
                setLoading(false);
                console.log(receipt);
                dispatch(fetchData(blockchain.account));
            });
        },
        toggleAllowToMintStatus: () => {
            setLoading(true);
            blockchain.erc721A_MerkleTree.methods.toggleAllowToMintStatus().send({
                from: blockchain.account
            }).once("error", (err) => {
                setLoading(false);
                console.log(err);
            }).then((receipt) => {
                setLoading(false);
                console.log(receipt);
                dispatch(fetchData(blockchain.account));
            });
        },
        toggleFreeMint: () => {
            setLoading(true);
            blockchain.erc721A_MerkleTree.methods.toggleFreeMint().send({
                from: blockchain.account
            }).once("error", (err) => {
                setLoading(false);
                console.log(err);
            }).then((receipt) => {
                setLoading(false);
                console.log(receipt);
                dispatch(fetchData(blockchain.account));
            });
        },
        toggleWhitelistedOnlyStatus: () => {
            setLoading(true);
            blockchain.erc721A_MerkleTree.methods.toggleWhitelistedOnlyStatus().send({
                from: blockchain.account
            }).once("error", (err) => {
                setLoading(false);
                console.log(err);
            }).then((receipt) => {
                setLoading(false);
                console.log(receipt);
                dispatch(fetchData(blockchain.account));
            });
        },
        updateRoot: (_root) => {
            setLoading(true);
            blockchain.erc721A_MerkleTree.methods.updateRoot(_root).send({
                from: blockchain.account
            }).on("error", (error) => {
                setLoading(false);
                console.log(error);
            }).then((receipt) => {
                setLoading(false);
                console.log(receipt);
                dispatch(fetchData(blockchain.account));
            });
        },
        changeName: async (_tokenId, _newName) => {
            setLoading(true);
            const nameChangeCost = await blockchain.erc721A_MerkleTree.methods.nameChangeCost().call();

            blockchain.erc721A_MerkleTree.methods.changeName(_tokenId, _newName).send({
                from: blockchain.account,
                value: nameChangeCost,
            }).on("error", (error) => {
                setLoading(false);
                console.log(error);
            }).then((receipt) => {
                setLoading(false);
                console.log(receipt);
                dispatch(fetchData(blockchain.account));
            });
        },
        mint: async (_mintAmount) => {
            setLoading(true);
            const proof = tree.getProof(keccak256(blockchain.account)).map(x => buf2hex(x.data));

            const mintCost = await blockchain.erc721A_MerkleTree.methods.mintCost().call();

            blockchain.erc721A_MerkleTree.methods.mint(_mintAmount, proof).send({
                from: blockchain.account,
                value: mintCost,
            }).on("error", (error) => {
                setLoading(false);
                console.log(error);
            }).then((receipt) => {
                setLoading(false);
                console.log(receipt);
                dispatch(fetchData(blockchain.account));
            });
        },
        isNameReserved: (_name) => {
            setLoading(true);
            blockchain.cExtractERC721A.methods.isNameReserved(_name).call({
                from: blockchain.account
            }).then((result) => {
                setLoading(false);
                console.log(result);
                setIsNameReserved_erc721AMerkleTree(result);
                dispatch(fetchData(blockchain.account));
            });
        },
        getTokenName: (_tokenId) => {
            setLoading(true);
            blockchain.erc721A_MerkleTree.methods.getTokenName(_tokenId).call({
                from: blockchain.account
            }).then((result) => {
                setLoading(false);
                console.log(result);
                setTokenName_erc721AMerkleTree(result);
                dispatch(fetchData(blockchain.account));
            });
        },
        isValid: (_proof) => {
            setLoading(true);
            blockchain.erc721A_MerkleTree.methods.isValid(_proof).call({
                from: blockchain.account
            }).then((result) => {
                setLoading(false);
                console.log(result);
                setIsValid_erc721AMerkleTree(result);
                dispatch(fetchData(blockchain.account));
            });
        },
        balanceOf: (_address) => {
            setLoading(true);
            blockchain.erc721A_MerkleTree.methods.balanceOf(_address).call({
                from: blockchain.account
            }).then((result) => {
                setLoading(false);
                console.log(result);
                setBalance_erc721AMerkleTree(result);
                dispatch(fetchData(blockchain.account));
            });
        },
        ownerOf: (_tokenId) => {
            setLoading(true);
            blockchain.erc721A_MerkleTree.methods.ownerOf(_tokenId).call({
                from: blockchain.account
            }).then((result) => {
                setLoading(false);
                console.log(result);
                setOwnerAddress_erc721AMerkleTree(result);
                dispatch(fetchData(blockchain.account));
            });
        }
    };

    const RewardsTokenMethods = {
        pause: () => {
            setLoading(true);
            blockchain.rewardsToken.methods.pause().send({
                from: blockchain.account,
            }).on("error", (error) => {
                setLoading(false);
                console.log(error);
            }).then((receipt) => {
                setLoading(false);
                console.log(receipt);
                dispatch(fetchData(blockchain.account));
            })
        },
        unpause: () => {
            setLoading(true);
            blockchain.rewardsToken.methods.unpause().send({
                from: blockchain.account,
            }).on("error", (error) => {
                setLoading(false);
                console.log(error);
            }).then((receipt) => {
                setLoading(false);
                console.log(receipt);
                dispatch(fetchData(blockchain.account));
            })
        },
        mint: (_to, _amount) => {
            setLoading(true);
            blockchain.rewardsToken.methods.mint(_to, _amount).send({
                from: blockchain.account
            }).on("error", (error) => {
                setLoading(false);
                console.log(error);
            }).then((receipt) => {
                setLoading(false);
                console.log(receipt);
                dispatch(fetchData(blockchain.account));
            });
        },
        balanceOf: (_address) => {
            setLoading(true);
            blockchain.rewardsToken.methods.balanceOf(_address).call({
                from: blockchain.account
            }).then((result) => {
                setLoading(false);
                console.log(result);
                setBalance_rewards(result);
                dispatch(fetchData(blockchain.account));
            });
        },
    };

    const StakesTokenMethods = {
        pause: () => {
            setLoading(true);
            blockchain.stakesToken.methods.pause().send({
                from: blockchain.account,
            }).on("error", (error) => {
                setLoading(false);
                console.log(error);
            }).then((receipt) => {
                setLoading(false);
                console.log(receipt);
                dispatch(fetchData(blockchain.account));
            })
        },
        unpause: () => {
            setLoading(true);
            blockchain.stakesToken.methods.unpause().send({
                from: blockchain.account,
            }).on("error", (error) => {
                setLoading(false);
                console.log(error);
            }).then((receipt) => {
                setLoading(false);
                console.log(receipt);
                dispatch(fetchData(blockchain.account));
            })
        },
        mint: (_to, _amount) => {
            setLoading(true);
            blockchain.stakesToken.methods.mint(_to, _amount).send({
                from: blockchain.account
            }).on("error", (error) => {
                setLoading(false);
                console.log(error);
            }).then((receipt) => {
                setLoading(false);
                console.log(receipt);
                dispatch(fetchData(blockchain.account));
            });
        },
        balanceOf: (_address) => {
            setLoading(true);
            blockchain.stakesToken.methods.balanceOf(_address).call({
                from: blockchain.account
            }).then((result) => {
                setLoading(false);
                console.log(result);
                setBalance_stakes(result);
                dispatch(fetchData(blockchain.account));
            });
        },
    };

    const ERC20StakingMethods = {
        stake: (_amount) => {
            setLoading(true);
            blockchain.stakesToken.methods.approve(blockchain.erc20Staking._address, _amount).send({
                from: blockchain.account,
            }).on("error", (error) => {
                setLoading(false);
                console.log(error);
            }).then((receipt) => {
                console.log(receipt);
                blockchain.erc20Staking.methods.stake(_amount).send({
                    from: blockchain.account,
                }).on("error", (error) => {
                    setLoading(false);
                    console.log(error);
                }).then((receipt) => {
                    setLoading(false);
                    console.log(receipt);
                    dispatch(fetchData(blockchain.account));
                });
            });
        },
        withdraw: (_amount) => {
            setLoading(true);
            blockchain.erc20Staking.methods.withdraw(_amount).send({
                from: blockchain.account,
            }).on("error", (error) => {
                setLoading(false);
                console.log(error);
            }).then((receipt) => {
                setLoading(false);
                console.log(receipt);
                dispatch(fetchData(blockchain.account));
            });
        },
        getReward: () => {
            setLoading(true);
            blockchain.erc20Staking.methods.getReward().send({
                from: blockchain.account,
            }).on("error", (error) => {
                setLoading(false);
                console.log(error);
            }).then((receipt) => {
                setLoading(false);
                console.log(receipt);
                dispatch(fetchData(blockchain.account));
            });
        },
        balances: (_address) => {
            setLoading(true);
            blockchain.erc20Staking.methods.balances(_address).call({
                from: blockchain.account,
            }).then((result) => {
                setLoading(false);
                console.log(result);
                setBalances_erc20Staking(result);
                dispatch(fetchData(blockchain.account));
            });
        },
        earned: (_address) => {
            setLoading(true);
            blockchain.erc20Staking.methods.earned(_address).call({
                from: blockchain.account,
            }).then((result) => {
                setLoading(false);
                console.log(result);
                setEarned_erc20Staking(result);
                dispatch(fetchData(blockchain.account));
            });
        },
    };

    const ERC721StakingMethods = {
        stake: (_tokenId) => {
            setLoading(true);
            blockchain.cExtractERC721A.methods.approve(blockchain.erc721Staking._address, _tokenId).send({
                from: blockchain.account,
            }).on("error", (error) => {
                setLoading(false);
                console.log(error);
            }).then((receipt) => {
                console.log(receipt);
                blockchain.erc721Staking.methods.stake([_tokenId]).send({
                    from: blockchain.account,
                }).on("error", (error) => {
                    setLoading(false);
                    console.log(error);
                }).then((receipt) => {
                    setLoading(false);
                    console.log(receipt);
                    dispatch(fetchData(blockchain.account));
                });
            });
        },
        withdraw: (_tokenId) => {
            setLoading(true);
            blockchain.erc721Staking.methods.withdraw([_tokenId]).send({
                from: blockchain.account,
            }).on("error", (error) => {
                setLoading(false);
                console.log(error);
            }).then((receipt) => {
                setLoading(false);
                console.log(receipt);
                dispatch(fetchData(blockchain.account));
            });
        },
        getReward: () => {
            setLoading(true);
            blockchain.erc721Staking.methods.getReward().send({
                from: blockchain.account,
            }).on("error", (error) => {
                setLoading(false);
                console.log(error);
            }).then((receipt) => {
                setLoading(false);
                console.log(receipt);
                dispatch(fetchData(blockchain.account));
            });
        },
        balances: (_address) => {
            setLoading(true);
            blockchain.erc721Staking.methods.balances(_address).call({
                from: blockchain.account,
            }).then((result) => {
                setLoading(false);
                console.log(result);
                setBalances_erc721Staking(result);
                dispatch(fetchData(blockchain.account));
            });
        },
        earned: (_address) => {
            setLoading(true);
            blockchain.erc721Staking.methods.earned(_address).call({
                from: blockchain.account,
            }).then((result) => {
                setLoading(false);
                console.log(result);
                setEarned_erc721Staking(result);
                dispatch(fetchData(blockchain.account));
            });
        },
    };

    useEffect(() => {
        if (blockchain.account !== "" &&
        // blockchain.cExtractERC721A !== null &&
        blockchain.erc721A_MerkleTree !== null &&
        blockchain.erc20Staking !== null &&
        blockchain.erc721Staking !== null &&
        blockchain.stakesToken !== null &&
        blockchain.rewardsToken !== null) {
            dispatch(fetchData(blockchain.account));
        }
    }, [/*blockchain.cExtractERC721A, */blockchain.erc721A_MerkleTree, blockchain.erc20Staking, blockchain.erc721Staking, blockchain.stakesToken, blockchain.rewardsToken]);

    return (
        <gs.Screen>
            {blockchain.account === "" ||
            // blockchain.cExtractERC721A === null ||
            blockchain.erc721A_MerkleTree == null ||
            blockchain.erc20Staking === null ||
            blockchain.erc721Staking === null ||
            blockchain.stakesToken === null ||
            blockchain.rewardsToken === null ? (
                <gs.Container flex={1} ai={"center"} jc={"center"}>
                <gs.TextTitle>Login with Metamask</gs.TextTitle>
                <gs.SpacerSmall />
                <gs.ButtonStyled onClick={(e) => {
                    e.preventDefault();
                    dispatch(connect());
                }}>
                    CONNECT
                </gs.ButtonStyled>
                <gs.SpacerXSmall />
                {blockchain.errorMsg != "" ? (
                    <gs.TextDescription>{blockchain.errorMsg}</gs.TextDescription>
                ) : null}
                </gs.Container>
            ) : (
                <gs.Container ai={"left"} style={{ padding: "24px" }}>
                    <gs.TextTitle>Welcome to the Game, {blockchain.account}</gs.TextTitle>
                    <gs.SpacerSmall />
                    <gs.Container fd={"row"} style={{ flexWrap: "wrap" }}>
                        {/* <gs.Container style={{ padding: "48px" }}>
                            <gs.TextSubTitle style={{ color: "cyan", fontSize: "20px" }}><u>CExtractERC721A Function: </u></gs.TextSubTitle>
                            <gs.SpacerMedium />
                            <gs.Container ai={"center"}>
                                <gs.TextSubTitle>Max Supply: {data.cExtractERC721AState.maxSupply}</gs.TextSubTitle>
                                <gs.SpacerXSmall />
                                <gs.TextSubTitle>Max Mint Amount Per Transaction: {data.cExtractERC721AState.maxMintAmountPerTrx}</gs.TextSubTitle>
                                <gs.SpacerXSmall />
                                <gs.TextSubTitle>Max Amount Per Wallet: {data.cExtractERC721AState.maxAmountPerWallet}</gs.TextSubTitle>
                                <gs.SpacerXSmall />
                                <gs.TextSubTitle>Mint Is Active: {data.cExtractERC721AState.mintIsActive.toString().toUpperCase()}</gs.TextSubTitle>
                                <gs.SpacerXSmall />
                                <gs.TextSubTitle>Whitelist Active: {data.cExtractERC721AState.whitelistActive.toString().toUpperCase()}</gs.TextSubTitle>
                                <gs.SpacerXSmall />
                            </gs.Container>
                            <gs.SpacerMedium />
                            {data.cExtractERC721AState.isAdmin ? (
                                <div>
                                    <gs.Container fd={"row"}>
                                        <gs.TextSubTitle>Toggle Mint Status: </gs.TextSubTitle>
                                        <gs.SpacerXSmall />
                                        <gs.ButtonStyled
                                            disabled={loading}
                                            onClick={(e) => {
                                                e.preventDefault();
                                                CExtractERC721AMethods.toggleMintStatus();
                                            }}
                                        >
                                            Update
                                        </gs.ButtonStyled>
                                    </gs.Container>
                                    <gs.SpacerLarge />
                                    <gs.Container fd={"row"}>
                                        <gs.TextSubTitle>Toggle Whitelist Status: </gs.TextSubTitle>
                                        <gs.SpacerXSmall />
                                        <gs.ButtonStyled
                                            disabled={loading}
                                            onClick={(e) => {
                                                e.preventDefault();
                                                CExtractERC721AMethods.toggleWhitelistStatus();
                                            }}
                                        >
                                            Update
                                        </gs.ButtonStyled>
                                    </gs.Container>
                                    <gs.SpacerLarge />
                                    <gs.Container fd={"row"}>
                                        <gs.Container>
                                            <gs.TextSubTitle>Set Max Supply: </gs.TextSubTitle>
                                            <gs.SpacerXSmall />
                                            <form>
                                                <input
                                                    type="number"
                                                    defaultValue={newMaxSupply_cExtract}
                                                    onChange={(e) => {
                                                        e.preventDefault();
                                                        console.log(e.currentTarget.value);
                                                        setNewMaxSupply_cExtract(e.currentTarget.value);
                                                    }}
                                                    min="0"
                                                    required
                                                />
                                            </form>
                                        </gs.Container>
                                        <gs.SpacerXSmall />
                                        <gs.ButtonStyled
                                            disabled={loading}
                                            onClick={(e) => {
                                                e.preventDefault();
                                                CExtractERC721AMethods.setMaxSupply(newMaxSupply_cExtract);
                                            }}
                                        >
                                            Update
                                        </gs.ButtonStyled>
                                    </gs.Container>
                                    <gs.SpacerLarge />
                                    <gs.Container fd={"row"}>
                                        <gs.Container>
                                            <gs.TextSubTitle> Set Max Mint Amount Per Trx: </gs.TextSubTitle>
                                            <gs.SpacerXSmall />
                                            <form>
                                                <input
                                                    type="number"
                                                    defaultValue={newMaxTrx_cExtract}
                                                    onChange={(e) => {
                                                        e.preventDefault();
                                                        setNewMaxTrx_cExtract(e.currentTarget.value);
                                                    }}
                                                    min="0"
                                                    required
                                                />
                                            </form>
                                        </gs.Container>
                                        <gs.SpacerXSmall />
                                        <gs.ButtonStyled
                                            disabled={loading}
                                            onClick={(e) => {
                                                e.preventDefault();
                                                CExtractERC721AMethods.setMaxMintAmountPerTrx(newMaxTrx_cExtract);
                                            }}
                                        >
                                            Update
                                        </gs.ButtonStyled>
                                    </gs.Container>
                                    <gs.SpacerLarge />
                                    <gs.Container fd={"row"}>
                                        <gs.Container>
                                            <gs.TextSubTitle>Set Max Amount Per Wallet: </gs.TextSubTitle>
                                            <gs.SpacerXSmall />
                                            <form>
                                                <input
                                                    type="number"
                                                    defaultValue={newMaxWallet_cExtract}
                                                    onChange={(e) => {
                                                        e.preventDefault();
                                                        setNewMaxWallet_cExtract(e.currentTarget.value);
                                                    }}
                                                    min="0"
                                                    required
                                                />
                                            </form>
                                        </gs.Container>
                                        <gs.SpacerXSmall />
                                        <gs.ButtonStyled
                                            disabled={loading}
                                            onClick={(e) => {
                                                e.preventDefault();
                                                CExtractERC721AMethods.setMaxAmountPerWallet(newMaxWallet_cExtract);
                                            }}
                                        >
                                            Update
                                        </gs.ButtonStyled>
                                    </gs.Container>
                                    <gs.SpacerLarge />
                                    <gs.Container fd={"row"}>
                                        <gs.Container>
                                            <gs.TextSubTitle>Grant Minter Role: </gs.TextSubTitle>
                                            <gs.SpacerXSmall />
                                            <form>
                                                <input
                                                    type="text"
                                                    defaultValue={grantMinterAddress_cExtract}
                                                    onChange={(e) => {
                                                        e.preventDefault();
                                                        setGrantMinterAddress_cExtract(e.currentTarget.value);
                                                    }}
                                                    required
                                                />
                                            </form>
                                        </gs.Container>
                                        <gs.SpacerXSmall />
                                        <gs.ButtonStyled
                                            disabled={loading}
                                            onClick={(e) => {
                                                e.preventDefault();
                                                CExtractERC721AMethods.grantMinter(grantMinterAddress_cExtract);
                                            }}
                                        >
                                            Grant
                                        </gs.ButtonStyled>
                                    </gs.Container>
                                    <gs.SpacerLarge />
                                    <gs.Container fd={"row"}>
                                        <gs.Container>
                                            <gs.TextSubTitle>Grant Whitelist Role: </gs.TextSubTitle>
                                            <gs.SpacerXSmall />
                                            <form>
                                                <input
                                                    type="text"
                                                    defaultValue={grantWhitelistAddress_cExtract}
                                                    onChange={(e) => {
                                                        e.preventDefault();
                                                        setGrantWhitelistAddress_cExtract(e.currentTarget.value);
                                                    }}
                                                    required
                                                />
                                            </form>
                                        </gs.Container>
                                        <gs.SpacerXSmall />
                                        <gs.ButtonStyled
                                            disabled={loading}
                                            onClick={(e) => {
                                                e.preventDefault();
                                                CExtractERC721AMethods.addWhitelistAccount(grantMinterAddress_cExtract);
                                            }}
                                        >
                                            Grant
                                        </gs.ButtonStyled>
                                    </gs.Container>
                                    <gs.SpacerLarge />
                                    <gs.Container fd={"row"}>
                                        <gs.Container>
                                            <gs.TextSubTitle>Revoke Minter Role: </gs.TextSubTitle>
                                            <gs.SpacerXSmall />
                                            <form>
                                                <input
                                                    type="text"
                                                    defaultValue={revokeMinterAddress_cExtract}
                                                    onChange={(e) => {
                                                        e.preventDefault();
                                                        setRevokeMinterAddress_cExtract(e.currentTarget.value);
                                                    }}
                                                    required
                                                />
                                            </form>
                                        </gs.Container>
                                        <gs.SpacerXSmall />
                                        <gs.ButtonStyled
                                            disabled={loading}
                                            onClick={(e) => {
                                                e.preventDefault();
                                                CExtractERC721AMethods.removeMinter(revokeMinterAddress_cExtract);
                                            }}
                                        >
                                            Revoke
                                        </gs.ButtonStyled>
                                    </gs.Container>
                                    <gs.SpacerLarge />
                                    <gs.Container fd={"row"}>
                                        <gs.Container>
                                            <gs.TextSubTitle>Revoke Whitelist Role: </gs.TextSubTitle>
                                            <gs.SpacerXSmall />
                                            <form>
                                                <input
                                                    type="text"
                                                    defaultValue={revokeWhitelistAddress_cExtract}
                                                    onChange={(e) => {
                                                        e.preventDefault();
                                                        setRevokeWhitelistAddress_cExtract(e.currentTarget.value);
                                                    }}
                                                    required
                                                />
                                            </form>
                                        </gs.Container>
                                        <gs.SpacerXSmall />
                                        <gs.ButtonStyled
                                            disabled={loading}
                                            onClick={(e) => {
                                                e.preventDefault();
                                                CExtractERC721AMethods.removeWhitelistAccount(revokeWhitelistAddress_cExtract);
                                            }}
                                        >
                                            Revoke
                                        </gs.ButtonStyled>
                                    </gs.Container>
                                    <gs.SpacerLarge />
                                </div>
                            ) : null }
                            {data.cExtractERC721AState.mintIsActive && data.cExtractERC721AState.isMinter ? (
                                <div>
                                    <gs.Container fd={"row"}>
                                        <gs.Container>
                                            <gs.TextSubTitle>Mint Amount: </gs.TextSubTitle>
                                            <gs.SpacerXSmall />
                                            <form>
                                                <input
                                                    type="number"
                                                    defaultValue={mintAmount_cExtract}
                                                    onChange={(e) => {
                                                        e.preventDefault();
                                                        setMintAmount_cExtract(e.currentTarget.value);
                                                    }}
                                                    min="1"
                                                    required
                                                />
                                            </form>
                                        </gs.Container>
                                        <gs.SpacerXSmall />
                                        <gs.ButtonStyled
                                            disabled={loading}
                                            onClick={(e) => {
                                                e.preventDefault();
                                                CExtractERC721AMethods.mint(mintAmount_cExtract);
                                            }}
                                        >
                                            Mint
                                        </gs.ButtonStyled>
                                    </gs.Container>
                                    <gs.SpacerLarge />
                                </div>
                            ) : null }
                            {data.cExtractERC721AState.mintIsActive && data.cExtractERC721AState.whitelistActive && data.cExtractERC721AState.isWhitelister ? (
                                <div>
                                    <gs.Container fd={"row"}>
                                        <gs.Container>
                                            <gs.TextSubTitle>Whitelist Mint Amount: </gs.TextSubTitle>
                                            <gs.SpacerXSmall />
                                            <form>
                                                <input
                                                    type="number"
                                                    defaultValue={whiteListMintAmount_cExtract}
                                                    onChange={(e) => {
                                                        e.preventDefault();
                                                        setWhiteListMintAmount_cExtract(e.currentTarget.value);
                                                    }}
                                                    min="1"
                                                    required
                                                />
                                            </form>
                                        </gs.Container>
                                        <gs.SpacerXSmall />
                                        <gs.ButtonStyled
                                            disabled={loading}
                                            onClick={(e) => {
                                                e.preventDefault();
                                                CExtractERC721AMethods.whitelistMint(whiteListMintAmount_cExtract);
                                            }}
                                        >
                                            Mint
                                        </gs.ButtonStyled>
                                    </gs.Container>
                                    <gs.SpacerLarge />
                                </div>
                            ) : null }
                            <gs.Container fd={"row"}>
                                <gs.Container>
                                    <gs.TextSubTitle>Balance Of: </gs.TextSubTitle>
                                    <gs.SpacerXSmall />
                                    <form>
                                        <input
                                            type="text"
                                            defaultValue={balanceOfAddress_cExtract}
                                            onChange={(e) => {
                                                e.preventDefault();
                                                setBalanceOfAddress_cExtract(e.currentTarget.value);
                                            }}
                                            required
                                        />
                                    </form>
                                    <gs.SpacerXSmall />
                                    <gs.TextSubTitle>Amount: {balance_cExtract} </gs.TextSubTitle>
                                </gs.Container>
                                <gs.SpacerXSmall />
                                <gs.ButtonStyled
                                    disabled={loading}
                                    onClick={(e) => {
                                        e.preventDefault();
                                        CExtractERC721AMethods.balanceOf(balanceOfAddress_cExtract);
                                    }}
                                >
                                    Check
                                </gs.ButtonStyled>
                            </gs.Container>
                            <gs.SpacerLarge />
                            <gs.Container fd={"row"}>
                                <gs.Container>
                                    <gs.TextSubTitle>Owner Of: </gs.TextSubTitle>
                                    <gs.SpacerXSmall />
                                    <form>
                                        <input
                                            type="number"
                                            defaultValue={ownerOfTokenId_cExtract}
                                            onChange={(e) => {
                                                e.preventDefault();
                                                setOwnerOfTokenId_cExtract(e.currentTarget.value);
                                            }}
                                            min="0"
                                            required
                                        />
                                    </form>
                                    <gs.SpacerXSmall />
                                    <gs.TextSubTitle>Owner: {ownerOfAddress_cExtract} </gs.TextSubTitle>
                                </gs.Container>
                                <gs.SpacerXSmall />
                                <gs.ButtonStyled
                                    disabled={loading}
                                    onClick={(e) => {
                                        e.preventDefault();
                                        CExtractERC721AMethods.ownerOf(ownerOfTokenId_cExtract);
                                    }}
                                >
                                    Check
                                </gs.ButtonStyled>
                            </gs.Container>
                            <gs.SpacerLarge />
                        </gs.Container> */}
                        <gs.Container style={{ padding: "48px" }}>
                            <gs.TextSubTitle style={{ color: "cyan", fontSize: "20px" }}><u>ERC721A_MerkleTree Function: </u></gs.TextSubTitle>
                            <gs.SpacerMedium />
                            <gs.Container ai={"center"}>
                                <gs.TextSubTitle>Mint Cost: {data.erc721A_MerkleTreeState.mintCost}</gs.TextSubTitle>
                                <gs.SpacerXSmall />
                                <gs.TextSubTitle>Name Change Cost: {data.erc721A_MerkleTreeState.nameChangeCost}</gs.TextSubTitle>
                                <gs.SpacerXSmall />
                                <gs.TextSubTitle>Max Supply: {data.erc721A_MerkleTreeState.maxSupply}</gs.TextSubTitle>
                                <gs.SpacerXSmall />
                                <gs.TextSubTitle>Max Mint Amount Per Transaction: {data.erc721A_MerkleTreeState.maxMintAmountPerTrx}</gs.TextSubTitle>
                                <gs.SpacerXSmall />
                                <gs.TextSubTitle>Max Amount Per Wallet: {data.erc721A_MerkleTreeState.maxAmountPerWallet}</gs.TextSubTitle>
                                <gs.SpacerXSmall />
                                <gs.TextSubTitle>Allow To Mint: {data.erc721A_MerkleTreeState.allowToMint.toString().toUpperCase()}</gs.TextSubTitle>
                                <gs.SpacerXSmall />
                                <gs.TextSubTitle>Free To Mint: {data.erc721A_MerkleTreeState.freeToMint.toString().toUpperCase()}</gs.TextSubTitle>
                                <gs.SpacerXSmall />
                                <gs.TextSubTitle>Whitelisted Only: {data.erc721A_MerkleTreeState.whitelistedOnly.toString().toUpperCase()}</gs.TextSubTitle>
                                <gs.SpacerXSmall />
                            </gs.Container>
                            <gs.SpacerMedium />
                            {data.erc721A_MerkleTreeState.isOwner ? (
                                <div>
                                    <gs.Container fd={"row"}>
                                        <gs.Container>
                                            <gs.TextSubTitle>Update Base URI: </gs.TextSubTitle>
                                            <gs.SpacerXSmall />
                                            <form>
                                                <input
                                                    type="text"
                                                    defaultValue={newURI_erc721AMerkleTree}
                                                    onChange={(e) => {
                                                        e.preventDefault();
                                                        setNewURI_erc721AMerkleTree(e.currentTarget.value);
                                                    }}
                                                    required
                                                />
                                            </form>
                                        </gs.Container>
                                        <gs.SpacerXSmall />
                                        <gs.ButtonStyled
                                            disabled={loading}
                                            onClick={(e) => {
                                                e.preventDefault();
                                                ERC721A_MerkleTreeMethods.updateBaseURI(newURI_erc721AMerkleTree);
                                            }}
                                        >
                                            Update
                                        </gs.ButtonStyled>
                                    </gs.Container>
                                    <gs.SpacerLarge />
                                    <gs.Container fd={"row"}>
                                        <gs.Container>
                                            <gs.TextSubTitle>Set Mint Cost: </gs.TextSubTitle>
                                            <gs.SpacerXSmall />
                                            <form>
                                                <input
                                                    type="number"
                                                    defaultValue={newMintCost_erc721AMerkleTree}
                                                    onChange={(e) => {
                                                        e.preventDefault();
                                                        setNewMintCost_erc721AMerkleTree(e.currentTarget.value);
                                                    }}
                                                    min="0"
                                                    required
                                                />
                                            </form>
                                        </gs.Container>
                                        <gs.SpacerXSmall />
                                        <gs.ButtonStyled
                                            disabled={loading}
                                            onClick={(e) => {
                                                e.preventDefault();
                                                ERC721A_MerkleTreeMethods.setMintCost(newMintCost_erc721AMerkleTree);
                                            }}
                                        >
                                            Update
                                        </gs.ButtonStyled>
                                    </gs.Container>
                                    <gs.SpacerLarge />
                                    <gs.Container fd={"row"}>
                                        <gs.Container>
                                            <gs.TextSubTitle>Set Name Change Cost: </gs.TextSubTitle>
                                            <gs.SpacerXSmall />
                                            <form>
                                                <input
                                                    type="number"
                                                    defaultValue={newNameChangeCost_erc721AMerkleTree}
                                                    onChange={(e) => {
                                                        e.preventDefault();
                                                        setNewNameChangeCost_erc721AMerkleTree(e.currentTarget.value);
                                                    }}
                                                    min="0"
                                                    required
                                                />
                                            </form>
                                        </gs.Container>
                                        <gs.SpacerXSmall />
                                        <gs.ButtonStyled
                                            disabled={loading}
                                            onClick={(e) => {
                                                e.preventDefault();
                                                ERC721A_MerkleTreeMethods.setNameChangeCost(newNameChangeCost_erc721AMerkleTree);
                                            }}
                                        >
                                            Update
                                        </gs.ButtonStyled>
                                    </gs.Container>
                                    <gs.SpacerLarge />
                                    <gs.Container fd={"row"}>
                                        <gs.Container>
                                            <gs.TextSubTitle>Set Max Supply: </gs.TextSubTitle>
                                            <gs.SpacerXSmall />
                                            <form>
                                                <input
                                                    type="number"
                                                    defaultValue={newMaxSupply_erc721AMerkleTree}
                                                    onChange={(e) => {
                                                        e.preventDefault();
                                                        setNewMaxSupply_erc721AMerkleTree(e.currentTarget.value);
                                                    }}
                                                    min="0"
                                                    required
                                                />
                                            </form>
                                        </gs.Container>
                                        <gs.SpacerXSmall />
                                        <gs.ButtonStyled
                                            disabled={loading}
                                            onClick={(e) => {
                                                e.preventDefault();
                                                ERC721A_MerkleTreeMethods.setMaxSupply(newMaxSupply_erc721AMerkleTree);
                                            }}
                                        >
                                            Update
                                        </gs.ButtonStyled>
                                    </gs.Container>
                                    <gs.SpacerLarge />
                                    <gs.Container fd={"row"}>
                                        <gs.Container>
                                            <gs.TextSubTitle>Set Max Mint Amount Per Trx: </gs.TextSubTitle>
                                            <gs.SpacerXSmall />
                                            <form>
                                                <input
                                                    type="number"
                                                    defaultValue={newMaxMintAmountPerTrx_erc721AMerkleTree}
                                                    onChange={(e) => {
                                                        e.preventDefault();
                                                        setNewMaxMintAmountPerTrx_erc721AMerkleTree(e.currentTarget.value);
                                                    }}
                                                    min="0"
                                                    required
                                                />
                                            </form>
                                        </gs.Container>
                                        <gs.SpacerXSmall />
                                        <gs.ButtonStyled
                                            disabled={loading}
                                            onClick={(e) => {
                                                e.preventDefault();
                                                ERC721A_MerkleTreeMethods.setMaxMintAmountPerTrx(newMaxMintAmountPerTrx_erc721AMerkleTree);
                                            }}
                                        >
                                            Update
                                        </gs.ButtonStyled>
                                    </gs.Container>
                                    <gs.SpacerLarge />
                                    <gs.Container fd={"row"}>
                                        <gs.Container>
                                            <gs.TextSubTitle>Set Max Amount Per Wallet: </gs.TextSubTitle>
                                            <gs.SpacerXSmall />
                                            <form>
                                                <input
                                                    type="number"
                                                    defaultValue={newMaxAmountPerWallet_erc721AMerkleTree}
                                                    onChange={(e) => {
                                                        e.preventDefault();
                                                        setNewMaxAmountPerWallet_erc721AMerkleTree(e.currentTarget.value);
                                                    }}
                                                    min="0"
                                                    required
                                                />
                                            </form>
                                        </gs.Container>
                                        <gs.SpacerXSmall />
                                        <gs.ButtonStyled
                                            disabled={loading}
                                            onClick={(e) => {
                                                e.preventDefault();
                                                ERC721A_MerkleTreeMethods.setMaxAmountPerWallet(newMaxAmountPerWallet_erc721AMerkleTree);
                                            }}
                                        >
                                            Update
                                        </gs.ButtonStyled>
                                    </gs.Container>
                                    <gs.SpacerLarge />
                                    <gs.Container fd={"row"}>
                                        <gs.TextSubTitle>Toggle Allow To Mint Status: </gs.TextSubTitle>
                                        <gs.SpacerXSmall />
                                        <gs.ButtonStyled
                                            disabled={loading}
                                            onClick={(e) => {
                                                e.preventDefault();
                                                ERC721A_MerkleTreeMethods.toggleAllowToMintStatus();
                                            }}
                                        >
                                            Update
                                        </gs.ButtonStyled>
                                    </gs.Container>
                                    <gs.SpacerLarge />
                                    <gs.Container fd={"row"}>
                                        <gs.TextSubTitle>Toggle Free Mint Status: </gs.TextSubTitle>
                                        <gs.SpacerXSmall />
                                        <gs.ButtonStyled
                                            disabled={loading}
                                            onClick={(e) => {
                                                e.preventDefault();
                                                ERC721A_MerkleTreeMethods.toggleFreeMint();
                                            }}
                                        >
                                            Update
                                        </gs.ButtonStyled>
                                    </gs.Container>
                                    <gs.SpacerLarge />
                                    <gs.Container fd={"row"}>
                                        <gs.TextSubTitle>Toggle Whitelisted Only Status: </gs.TextSubTitle>
                                        <gs.SpacerXSmall />
                                        <gs.ButtonStyled
                                            disabled={loading}
                                            onClick={(e) => {
                                                e.preventDefault();
                                                ERC721A_MerkleTreeMethods.toggleWhitelistedOnlyStatus();
                                            }}
                                        >
                                            Update
                                        </gs.ButtonStyled>
                                    </gs.Container>
                                    <gs.SpacerLarge />
                                    <gs.Container fd={"row"}>
                                        <gs.Container>
                                            <gs.TextSubTitle>Update Root: </gs.TextSubTitle>
                                            <gs.SpacerXSmall />
                                            <form>
                                                <input
                                                    type="text"
                                                    defaultValue={root_erc721AMerkleTree}
                                                    onChange={(e) => {
                                                        e.preventDefault();
                                                        setRoot_erc721AMerkleTree(e.currentTarget.value);
                                                    }}
                                                    required
                                                />
                                            </form>
                                        </gs.Container>
                                        <gs.SpacerXSmall />
                                        <gs.ButtonStyled
                                            disabled={loading}
                                            onClick={(e) => {
                                                e.preventDefault();
                                                ERC721A_MerkleTreeMethods.updateRoot(root_erc721AMerkleTree);
                                            }}
                                        >
                                            Update
                                        </gs.ButtonStyled>
                                    </gs.Container>
                                    <gs.SpacerLarge />
                                </div>
                            ) : null }
                            {data.erc721A_MerkleTreeState.allowToMint ? (
                                <div>
                                    <gs.Container fd={"row"}>
                                        <gs.Container>
                                            <gs.TextSubTitle>Mint Amount: </gs.TextSubTitle>
                                            <gs.SpacerXSmall />
                                            <form>
                                                <input
                                                    type="number"
                                                    defaultValue={mintAmount_erc721AMerkleTree}
                                                    onChange={(e) => {
                                                        e.preventDefault();
                                                        setMintAmount_erc721AMerkleTree(e.currentTarget.value);
                                                    }}
                                                    min="0"
                                                    required
                                                />
                                            </form>
                                        </gs.Container>
                                        <gs.SpacerXSmall />
                                        <gs.ButtonStyled
                                            disabled={loading}
                                            onClick={(e) => {
                                                e.preventDefault();
                                                ERC721A_MerkleTreeMethods.mint(mintAmount_erc721AMerkleTree);
                                            }}
                                        >
                                            Mint
                                        </gs.ButtonStyled>
                                    </gs.Container>
                                    <gs.SpacerLarge />
                                </div>
                            ) : null }
                            <gs.Container fd={"row"}>
                                <gs.Container>
                                    <gs.TextSubTitle>Change Name Of Token Id: </gs.TextSubTitle>
                                    <gs.SpacerXSmall />
                                    <gs.Container fd={"row"}>
                                        <gs.TextSubTitle>Token Id: </gs.TextSubTitle>
                                        <gs.SpacerXSmall />
                                        <form>
                                            <input
                                                type="text"
                                                defaultValue={tokenId_erc721AMerkleTree}
                                                onChange={(e) => {
                                                    e.preventDefault();
                                                    setTokenId_erc721AMerkleTree(e.currentTarget.value);
                                                }}
                                                required
                                            />
                                        </form>
                                    </gs.Container>
                                    <gs.SpacerXSmall />
                                    <gs.Container fd={"row"}>
                                        <gs.TextSubTitle>New Name: </gs.TextSubTitle>
                                        <gs.SpacerXSmall />
                                        <form>
                                            <input
                                                type="text"
                                                defaultValue={newName_erc721AMerkleTree}
                                                onChange={(e) => {
                                                    e.preventDefault();
                                                    setNewName_erc721AMerkleTree(e.currentTarget.value);
                                                }}
                                                required
                                            />
                                        </form>
                                    </gs.Container>
                                </gs.Container>
                                <gs.SpacerXSmall />
                                <gs.ButtonStyled
                                    disabled={loading}
                                    onClick={(e) => {
                                        e.preventDefault();
                                        ERC721A_MerkleTreeMethods.changeName(tokenId_erc721AMerkleTree, newName_erc721AMerkleTree);
                                    }}
                                >
                                    Update
                                </gs.ButtonStyled>
                            </gs.Container>
                            <gs.SpacerLarge />
                            <gs.Container fd={"row"}>
                                <gs.Container>
                                    <gs.TextSubTitle>Check Is Name Reserved: </gs.TextSubTitle>
                                    <gs.SpacerXSmall />
                                    <form>
                                        <input
                                            type="text"
                                            defaultValue={checkName_erc721AMerkleTree}
                                            onChange={(e) => {
                                                e.preventDefault();
                                                setCheckName_erc721AMerkleTree(e.currentTarget.value);
                                            }}
                                            required
                                        />
                                    </form>
                                    <gs.SpacerXSmall />
                                    <gs.TextSubTitle>IsReserved: {isNameReserved_erc721AMerkleTree.toString().toUpperCase()} </gs.TextSubTitle>
                                </gs.Container>
                                <gs.SpacerXSmall />
                                <gs.ButtonStyled
                                    disabled={loading}
                                    onClick={(e) => {
                                        e.preventDefault();
                                        ERC721A_MerkleTreeMethods.isNameReserved(checkName_erc721AMerkleTree);
                                    }}
                                >
                                    Check
                                </gs.ButtonStyled>
                            </gs.Container>
                            <gs.SpacerLarge />
                            <gs.Container fd={"row"}>
                                <gs.Container>
                                    <gs.TextSubTitle>Get Token Id Name: </gs.TextSubTitle>
                                    <gs.SpacerXSmall />
                                    <form>
                                        <input
                                            type="text"
                                            defaultValue={checkTokenId_erc721AMerkleTree}
                                            onChange={(e) => {
                                                e.preventDefault();
                                                setCheckTokenId_erc721AMerkleTree(e.currentTarget.value);
                                            }}
                                            required
                                        />
                                    </form>
                                    <gs.SpacerXSmall />
                                    <gs.TextSubTitle>Token Name: {tokenName_erc721AMerkleTree} </gs.TextSubTitle>
                                </gs.Container>
                                <gs.SpacerXSmall />
                                <gs.ButtonStyled
                                    disabled={loading}
                                    onClick={(e) => {
                                        e.preventDefault();
                                        ERC721A_MerkleTreeMethods.getTokenName(checkTokenId_erc721AMerkleTree);
                                    }}
                                >
                                    Get Token Name
                                </gs.ButtonStyled>
                            </gs.Container>
                            <gs.SpacerLarge />
                            <gs.Container fd={"row"}>
                                <gs.Container>
                                    <gs.TextSubTitle>Is Proof Valid: </gs.TextSubTitle>
                                    <gs.SpacerXSmall />
                                    <form>
                                        <input
                                            type="text"
                                            defaultValue={proof_erc721AMerkleTree}
                                            onChange={(e) => {
                                                e.preventDefault();
                                                setProof_erc721AMerkleTree(e.currentTarget.value);
                                            }}
                                            required
                                        />
                                    </form>
                                    <gs.SpacerXSmall />
                                    <gs.TextSubTitle>IsValid: {isValid_erc721AMerkleTree.toString().toUpperCase()} </gs.TextSubTitle>
                                </gs.Container>
                                <gs.SpacerXSmall />
                                <gs.ButtonStyled
                                    disabled={loading}
                                    onClick={(e) => {
                                        e.preventDefault();
                                        ERC721A_MerkleTreeMethods.isValid(proof_erc721AMerkleTree);
                                    }}
                                >
                                    Check
                                </gs.ButtonStyled>
                            </gs.Container>
                            <gs.SpacerLarge />
                            <gs.Container fd={"row"}>
                                <gs.Container>
                                    <gs.TextSubTitle>Balance Of: </gs.TextSubTitle>
                                    <gs.SpacerXSmall />
                                    <form>
                                        <input
                                            type="text"
                                            defaultValue={balanceOfAddress_erc721AMerkleTree}
                                            onChange={(e) => {
                                                e.preventDefault();
                                                setBalanceOfAddress_erc721AMerkleTree(e.currentTarget.value);
                                            }}
                                            required
                                        />
                                    </form>
                                    <gs.SpacerXSmall />
                                    <gs.TextSubTitle>Amount: {balance_erc721AMerkleTree} </gs.TextSubTitle>
                                </gs.Container>
                                <gs.SpacerXSmall />
                                <gs.ButtonStyled
                                    disabled={loading}
                                    onClick={(e) => {
                                        e.preventDefault();
                                        ERC721A_MerkleTreeMethods.balanceOf(balanceOfAddress_erc721AMerkleTree);
                                    }}
                                >
                                    Check
                                </gs.ButtonStyled>
                            </gs.Container>
                            <gs.SpacerLarge />
                            <gs.Container fd={"row"}>
                                <gs.Container>
                                    <gs.TextSubTitle>Owner Of: </gs.TextSubTitle>
                                    <gs.SpacerXSmall />
                                    <form>
                                        <input
                                            type="number"
                                            defaultValue={ownerOfTokenId_erc721AMerkleTree}
                                            onChange={(e) => {
                                                e.preventDefault();
                                                setOwnerOfTokenId_erc721AMerkleTree(e.currentTarget.value);
                                            }}
                                            min="0"
                                            required
                                        />
                                    </form>
                                    <gs.SpacerXSmall />
                                    <gs.TextSubTitle>Owner: {ownerAddress_erc721AMerkleTree} </gs.TextSubTitle>
                                </gs.Container>
                                <gs.SpacerXSmall />
                                <gs.ButtonStyled
                                    disabled={loading}
                                    onClick={(e) => {
                                        e.preventDefault();
                                        ERC721A_MerkleTreeMethods.ownerOf(ownerOfTokenId_erc721AMerkleTree);
                                    }}
                                >
                                    Check
                                </gs.ButtonStyled>
                            </gs.Container>
                            <gs.SpacerLarge />
                        </gs.Container>
                        <gs.Container style={{ padding: "48px" }}>
                            <gs.TextSubTitle style={{ color: "cyan", fontSize: "20px" }}><u>RewardsToken Function: </u></gs.TextSubTitle>
                            <gs.SpacerMedium />
                            <gs.Container ai={"center"}>
                                <gs.TextSubTitle>Paused: {data.rewardsTokenState.paused.toString().toUpperCase()}</gs.TextSubTitle>
                                <gs.SpacerXSmall />
                                <gs.TextSubTitle>Total Supply: {data.rewardsTokenState.totalSupply}</gs.TextSubTitle>
                                <gs.SpacerXSmall />
                                <gs.TextSubTitle>Balance: {data.rewardsTokenState.balance}</gs.TextSubTitle>
                                <gs.SpacerXSmall />
                            </gs.Container>
                            <gs.SpacerMedium />
                            {data.rewardsTokenState.isAdmin ? (
                                <div>
                                </div>
                            ) : null }
                            {data.rewardsTokenState.isPauser ? (
                                <div>
                                    <gs.Container fd={"row"}>
                                        <gs.TextSubTitle>Pause: </gs.TextSubTitle>
                                        <gs.SpacerXSmall />
                                        <gs.ButtonStyled
                                            disabled={loading}
                                            onClick={(e) => {
                                                e.preventDefault();
                                                RewardsTokenMethods.pause();
                                            }}
                                        >
                                            Pause
                                        </gs.ButtonStyled>
                                    </gs.Container>
                                    <gs.SpacerLarge />
                                    <gs.Container fd={"row"}>
                                        <gs.TextSubTitle>Unpause: </gs.TextSubTitle>
                                        <gs.SpacerXSmall />
                                        <gs.ButtonStyled
                                            disabled={loading}
                                            onClick={(e) => {
                                                e.preventDefault();
                                                RewardsTokenMethods.unpause();
                                            }}
                                        >
                                            Unpause
                                        </gs.ButtonStyled>
                                    </gs.Container>
                                    <gs.SpacerLarge />
                                </div>
                            ) : null }
                            {data.rewardsTokenState.isMinter && !data.rewardsTokenState.paused ? (
                                <div>
                                    <gs.Container fd={"row"}>
                                        <gs.Container>
                                            <gs.TextSubTitle>Mint To: </gs.TextSubTitle>
                                            <gs.SpacerXSmall />
                                            <form>
                                                <input
                                                    type="text"
                                                    defaultValue={mintTo_rewards}
                                                    onChange={(e) => {
                                                        e.preventDefault();
                                                        setMintTo_rewards(e.currentTarget.value);
                                                    }}
                                                    required
                                                />
                                            </form>
                                            <gs.SpacerXSmall />
                                            <gs.TextSubTitle>Mint Amount: </gs.TextSubTitle>
                                            <gs.SpacerXSmall />
                                            <form>
                                                <input
                                                    type="number"
                                                    defaultValue={mintAmount_rewards}
                                                    onChange={(e) => {
                                                        e.preventDefault();
                                                        setMintAmount_rewards(e.currentTarget.value);
                                                    }}
                                                    min="1"
                                                    required
                                                />
                                            </form>
                                        </gs.Container>
                                        <gs.SpacerXSmall />
                                        <gs.ButtonStyled
                                            disabled={loading}
                                            onClick={(e) => {
                                                e.preventDefault();
                                                RewardsTokenMethods.mint(mintTo_rewards, mintAmount_rewards);
                                            }}
                                        >
                                            Mint
                                        </gs.ButtonStyled>
                                    </gs.Container>
                                    <gs.SpacerLarge />
                                </div>
                            ) : null }
                            <gs.Container fd={"row"}>
                                <gs.Container>
                                    <gs.TextSubTitle>Balance Of: </gs.TextSubTitle>
                                    <gs.SpacerXSmall />
                                    <form>
                                        <input
                                            type="text"
                                            defaultValue={balanceOfAddress_rewards}
                                            onChange={(e) => {
                                                e.preventDefault();
                                                setBalanceOfAddress_rewards(e.currentTarget.value);
                                            }}
                                            required
                                        />
                                    </form>
                                    <gs.SpacerXSmall />
                                    <gs.TextSubTitle>Amount: {balance_rewards} </gs.TextSubTitle>
                                </gs.Container>
                                <gs.SpacerXSmall />
                                <gs.ButtonStyled
                                    disabled={loading}
                                    onClick={(e) => {
                                        e.preventDefault();
                                        RewardsTokenMethods.balanceOf(balanceOfAddress_rewards);
                                    }}
                                >
                                    Check
                                </gs.ButtonStyled>
                            </gs.Container>
                            <gs.SpacerLarge />
                        </gs.Container>
                        <gs.Container style={{ padding: "48px" }}>
                            <gs.TextSubTitle style={{ color: "cyan", fontSize: "20px" }}><u>StakesToken Function: </u></gs.TextSubTitle>
                            <gs.SpacerMedium />
                            <gs.Container ai={"center"}>
                                <gs.TextSubTitle>Paused: {data.stakesTokenState.paused.toString().toUpperCase()}</gs.TextSubTitle>
                                <gs.SpacerXSmall />
                                <gs.TextSubTitle>Total Supply: {data.stakesTokenState.totalSupply}</gs.TextSubTitle>
                                <gs.SpacerXSmall />
                                <gs.TextSubTitle>Balance: {data.stakesTokenState.balance}</gs.TextSubTitle>
                                <gs.SpacerXSmall />
                            </gs.Container>
                            <gs.SpacerMedium />
                            {data.stakesTokenState.isAdmin ? (
                                <div>
                                </div>
                            ) : null }
                            {data.stakesTokenState.isPauser ? (
                                <div>
                                    <gs.Container fd={"row"}>
                                        <gs.TextSubTitle>Pause: </gs.TextSubTitle>
                                        <gs.SpacerXSmall />
                                        <gs.ButtonStyled
                                            disabled={loading}
                                            onClick={(e) => {
                                                e.preventDefault();
                                                StakesTokenMethods.pause();
                                            }}
                                        >
                                            Pause
                                        </gs.ButtonStyled>
                                    </gs.Container>
                                    <gs.SpacerLarge />
                                    <gs.Container fd={"row"}>
                                        <gs.TextSubTitle>Unpause: </gs.TextSubTitle>
                                        <gs.SpacerXSmall />
                                        <gs.ButtonStyled
                                            disabled={loading}
                                            onClick={(e) => {
                                                e.preventDefault();
                                                StakesTokenMethods.unpause();
                                            }}
                                        >
                                            Unpause
                                        </gs.ButtonStyled>
                                    </gs.Container>
                                    <gs.SpacerLarge />
                                </div>
                            ) : null }
                            {data.stakesTokenState.isMinter && !data.stakesTokenState.paused ? (
                                <div>
                                    <gs.Container fd={"row"}>
                                        <gs.Container>
                                            <gs.TextSubTitle>Mint To: </gs.TextSubTitle>
                                            <gs.SpacerXSmall />
                                            <form>
                                                <input
                                                    type="text"
                                                    defaultValue={mintTo_stakes}
                                                    onChange={(e) => {
                                                        e.preventDefault();
                                                        setMintTo_stakes(e.currentTarget.value);
                                                    }}
                                                    required
                                                />
                                            </form>
                                            <gs.SpacerXSmall />
                                            <gs.TextSubTitle>Mint Amount: </gs.TextSubTitle>
                                            <gs.SpacerXSmall />
                                            <form>
                                                <input
                                                    type="number"
                                                    defaultValue={mintAmount_stakes}
                                                    onChange={(e) => {
                                                        e.preventDefault();
                                                        setMintAmount_stakes(e.currentTarget.value);
                                                    }}
                                                    min="1"
                                                    required
                                                />
                                            </form>
                                        </gs.Container>
                                        <gs.SpacerXSmall />
                                        <gs.ButtonStyled
                                            disabled={loading}
                                            onClick={(e) => {
                                                e.preventDefault();
                                                StakesTokenMethods.mint(mintTo_stakes, mintAmount_stakes);
                                            }}
                                        >
                                            Mint
                                        </gs.ButtonStyled>
                                    </gs.Container>
                                    <gs.SpacerLarge />
                                </div>
                            ) : null }
                            <gs.Container fd={"row"}>
                                <gs.Container>
                                    <gs.TextSubTitle>Balance Of: </gs.TextSubTitle>
                                    <gs.SpacerXSmall />
                                    <form>
                                        <input
                                            type="text"
                                            defaultValue={balanceOfAddress_stakes}
                                            onChange={(e) => {
                                                e.preventDefault();
                                                setBalanceOfAddress_stakes(e.currentTarget.value);
                                            }}
                                            required
                                        />
                                    </form>
                                    <gs.SpacerXSmall />
                                    <gs.TextSubTitle>Amount: {balance_stakes} </gs.TextSubTitle>
                                </gs.Container>
                                <gs.SpacerXSmall />
                                <gs.ButtonStyled
                                    disabled={loading}
                                    onClick={(e) => {
                                        e.preventDefault();
                                        StakesTokenMethods.balanceOf(balanceOfAddress_stakes);
                                    }}
                                >
                                    Check
                                </gs.ButtonStyled>
                            </gs.Container>
                            <gs.SpacerLarge />
                        </gs.Container>
                        <gs.Container style={{ padding: "48px" }}>
                            <gs.TextSubTitle style={{ color: "cyan", fontSize: "20px" }}><u>ERC20 Staking Function: </u></gs.TextSubTitle>
                            <gs.SpacerMedium />
                            <gs.Container ai={"center"}>
                                <gs.TextSubTitle>Balances: {data.erc20StakingState.balances}</gs.TextSubTitle>
                                <gs.SpacerXSmall />
                            </gs.Container>
                            <gs.SpacerMedium />
                            <gs.Container fd={"row"}>
                                <gs.Container>
                                    <gs.TextSubTitle>Stake: </gs.TextSubTitle>
                                    <gs.SpacerXSmall />
                                    <form>
                                        <input
                                            type="number"
                                            defaultValue={stakeAmount_erc20Staking}
                                            onChange={(e) => {
                                                e.preventDefault();
                                                setStakeAmount_erc20Staking(e.currentTarget.value);
                                            }}
                                            required
                                        />
                                    </form>
                                </gs.Container>
                                <gs.SpacerXSmall />
                                <gs.ButtonStyled
                                    disabled={loading}
                                    onClick={(e) => {
                                        e.preventDefault();
                                        ERC20StakingMethods.stake(stakeAmount_erc20Staking);
                                    }}
                                >
                                    Stake
                                </gs.ButtonStyled>
                            </gs.Container>
                            <gs.SpacerLarge />
                            <gs.Container fd={"row"}>
                                <gs.Container>
                                    <gs.TextSubTitle>Withdraw: </gs.TextSubTitle>
                                    <gs.SpacerXSmall />
                                    <form>
                                        <input
                                            type="number"
                                            defaultValue={withdrawAmount_erc20Staking}
                                            onChange={(e) => {
                                                e.preventDefault();
                                                setWithdrawAmount_erc20Staking(e.currentTarget.value);
                                            }}
                                            required
                                        />
                                    </form>
                                </gs.Container>
                                <gs.SpacerXSmall />
                                <gs.ButtonStyled
                                    disabled={loading}
                                    onClick={(e) => {
                                        e.preventDefault();
                                        ERC20StakingMethods.withdraw(withdrawAmount_erc20Staking);
                                    }}
                                >
                                    Withdraw
                                </gs.ButtonStyled>
                            </gs.Container>
                            <gs.SpacerLarge />
                            <gs.Container fd={"row"}>
                                <gs.TextSubTitle>Get Rewards: </gs.TextSubTitle>
                                <gs.SpacerXSmall />
                                <gs.ButtonStyled
                                    disabled={loading}
                                    onClick={(e) => {
                                        e.preventDefault();
                                        ERC20StakingMethods.getReward();
                                    }}
                                >
                                    Claim
                                </gs.ButtonStyled>
                            </gs.Container>
                            <gs.SpacerLarge />
                            <gs.Container fd={"row"}>
                                <gs.Container>
                                    <gs.TextSubTitle>Balances: </gs.TextSubTitle>
                                    <gs.SpacerXSmall />
                                    <form>
                                        <input
                                            type="text"
                                            defaultValue={balancesAddress_erc20Staking}
                                            onChange={(e) => {
                                                e.preventDefault();
                                                setBalancesAddress_erc20Staking(e.currentTarget.value);
                                            }}
                                            required
                                        />
                                    </form>
                                    <gs.SpacerXSmall />
                                    <gs.TextSubTitle>result: {balances_erc20Staking} </gs.TextSubTitle>
                                </gs.Container>
                                <gs.SpacerXSmall />
                                <gs.ButtonStyled
                                    disabled={loading}
                                    onClick={(e) => {
                                        e.preventDefault();
                                        ERC20StakingMethods.balances(balancesAddress_erc20Staking);
                                    }}
                                >
                                    Check
                                </gs.ButtonStyled>
                            </gs.Container>
                            <gs.SpacerLarge />
                            <gs.Container fd={"row"}>
                                <gs.Container>
                                    <gs.TextSubTitle>Earned: </gs.TextSubTitle>
                                    <gs.SpacerXSmall />
                                    <form>
                                        <input
                                            type="text"
                                            defaultValue={earnedAddress_erc20Staking}
                                            onChange={(e) => {
                                                e.preventDefault();
                                                setEarnedAddress_erc20Staking(e.currentTarget.value);
                                            }}
                                            required
                                        />
                                    </form>
                                    <gs.SpacerXSmall />
                                    <gs.TextSubTitle>result: {earned_erc20Staking} </gs.TextSubTitle>
                                </gs.Container>
                                <gs.SpacerXSmall />
                                <gs.ButtonStyled
                                    disabled={loading}
                                    onClick={(e) => {
                                        e.preventDefault();
                                        ERC20StakingMethods.earned(earnedAddress_erc20Staking);
                                    }}
                                >
                                    Check
                                </gs.ButtonStyled>
                            </gs.Container>
                            <gs.SpacerLarge />
                        </gs.Container>
                        <gs.Container style={{ padding: "48px" }}>
                            <gs.TextSubTitle style={{ color: "cyan", fontSize: "20px" }}><u>ERC721 Staking Function: </u></gs.TextSubTitle>
                            <gs.SpacerMedium />
                            <gs.Container ai={"center"}>
                                <gs.TextSubTitle>Balances: {data.erc721StakingState.balances}</gs.TextSubTitle>
                                <gs.SpacerXSmall />
                            </gs.Container>
                            <gs.SpacerMedium />
                            <gs.Container fd={"row"}>
                                <gs.Container>
                                    <gs.TextSubTitle>Stake: </gs.TextSubTitle>
                                    <gs.SpacerXSmall />
                                    <form>
                                        <input
                                            type="number"
                                            defaultValue={stakeId_erc721Staking}
                                            onChange={(e) => {
                                                e.preventDefault();
                                                setStakeId_erc721Staking(e.currentTarget.value);
                                            }}
                                            required
                                        />
                                    </form>
                                </gs.Container>
                                <gs.SpacerXSmall />
                                <gs.ButtonStyled
                                    disabled={loading}
                                    onClick={(e) => {
                                        e.preventDefault();
                                        ERC721StakingMethods.stake(stakeId_erc721Staking);
                                    }}
                                >
                                    Stake
                                </gs.ButtonStyled>
                            </gs.Container>
                            <gs.SpacerLarge />
                            <gs.Container fd={"row"}>
                                <gs.Container>
                                    <gs.TextSubTitle>Withdraw: </gs.TextSubTitle>
                                    <gs.SpacerXSmall />
                                    <form>
                                        <input
                                            type="number"
                                            defaultValue={withdrawId_erc721Staking}
                                            onChange={(e) => {
                                                e.preventDefault();
                                                setWithdrawId_erc721Staking(e.currentTarget.value);
                                            }}
                                            required
                                        />
                                    </form>
                                </gs.Container>
                                <gs.SpacerXSmall />
                                <gs.ButtonStyled
                                    disabled={loading}
                                    onClick={(e) => {
                                        e.preventDefault();
                                        ERC721StakingMethods.withdraw(withdrawId_erc721Staking);
                                    }}
                                >
                                    Withdraw
                                </gs.ButtonStyled>
                            </gs.Container>
                            <gs.SpacerLarge />
                            <gs.Container fd={"row"}>
                                <gs.TextSubTitle>Get Rewards: </gs.TextSubTitle>
                                <gs.SpacerXSmall />
                                <gs.ButtonStyled
                                    disabled={loading}
                                    onClick={(e) => {
                                        e.preventDefault();
                                        ERC721StakingMethods.getReward();
                                    }}
                                >
                                    Claim
                                </gs.ButtonStyled>
                            </gs.Container>
                            <gs.SpacerLarge />
                            <gs.Container fd={"row"}>
                                <gs.Container>
                                    <gs.TextSubTitle>Balances: </gs.TextSubTitle>
                                    <gs.SpacerXSmall />
                                    <form>
                                        <input
                                            type="text"
                                            defaultValue={balancesAddress_erc721Staking}
                                            onChange={(e) => {
                                                e.preventDefault();
                                                setBalancesAddress_erc721Staking(e.currentTarget.value);
                                            }}
                                            required
                                        />
                                    </form>
                                    <gs.SpacerXSmall />
                                    <gs.TextSubTitle>result: {balances_erc721Staking} </gs.TextSubTitle>
                                </gs.Container>
                                <gs.SpacerXSmall />
                                <gs.ButtonStyled
                                    disabled={loading}
                                    onClick={(e) => {
                                        e.preventDefault();
                                        ERC721StakingMethods.balances(balancesAddress_erc721Staking);
                                    }}
                                >
                                    Check
                                </gs.ButtonStyled>
                            </gs.Container>
                            <gs.SpacerLarge />
                            <gs.Container fd={"row"}>
                                <gs.Container>
                                    <gs.TextSubTitle>Earned: </gs.TextSubTitle>
                                    <gs.SpacerXSmall />
                                    <form>
                                        <input
                                            type="text"
                                            defaultValue={earnedAddress_erc721Staking}
                                            onChange={(e) => {
                                                e.preventDefault();
                                                setEarnedAddress_erc721Staking(e.currentTarget.value);
                                            }}
                                            required
                                        />
                                    </form>
                                    <gs.SpacerXSmall />
                                    <gs.TextSubTitle>result: {earned_erc721Staking} </gs.TextSubTitle>
                                </gs.Container>
                                <gs.SpacerXSmall />
                                <gs.ButtonStyled
                                    disabled={loading}
                                    onClick={(e) => {
                                        e.preventDefault();
                                        ERC721StakingMethods.earned(earnedAddress_erc721Staking);
                                    }}
                                >
                                    Check
                                </gs.ButtonStyled>
                            </gs.Container>
                            <gs.SpacerLarge />
                        </gs.Container>
                    </gs.Container>
                    <gs.Container jc={"center"} fd={"row"} style={{ flexWrap: "wrap" }}>
                        {/* {data.cExtractERC721AState.allCExtractsOwned.map((item, index) => {
                            return (
                                <gs.Container key={index} style={{ padding:"15px" }}>
                                    <Renderer cext={item} />
                                    <gs.SpacerXSmall />
                                    <gs.Container>
                                        <gs.TextDescription>ID: {item}</gs.TextDescription>
                                    </gs.Container>
                                </gs.Container>
                            );
                        })} */}

                        {data.erc721A_MerkleTreeState.allERC721AOwned.map((item, index) => {
                            return (
                                <gs.Container key={index} style={{ padding:"15px" }}>
                                    <Renderer cext={item.index}/>
                                    <gs.SpacerXSmall />
                                    <gs.Container>
                                        <gs.TextDescription>ID: {item.index}</gs.TextDescription>
                                        <gs.TextDescription>Item Name: {item.itemName}</gs.TextDescription>
                                        {/* <gs.TextDescription>TokenURI: {data.baseURI}{item}</gs.TextDescription> */}
                                    </gs.Container>
                                </gs.Container>
                            );
                        })}
                    </gs.Container>
                    {/* <gs.Container style={{ padding: "24px" }}>
                        <gs.TextSubTitle>Contract's Menthods: </gs.TextSubTitle>
                    </gs.Container> */}
                    {/* <gs.Container ai={"center"} style={{ padding: "24px" }}>
                        <gs.TextTitle>Welcome to the Game, {blockchain.account}</gs.TextTitle>
                        <gs.SpacerSmall />
                        {blockchain.account == data.owner ? (
                            <gs.Container ai={"center"} style={{ padding: "24px" }}>
                                <gs.ButtonStyled
                                    disabled={loading ? 1 : 0}
                                    onClick={(e) => {
                                        e.preventDefault();
                                        toggleMintStatus(blockchain.account);
                                    }}
                                >
                                    Toggle Mint NFT
                                </gs.ButtonStyled>
                                <gs.SpacerSmall />
                                <gs.ButtonStyled
                                    disabled={loading ? 1 : 0}
                                    onClick={(e) => {
                                        e.preventDefault();
                                        setCost(blockchain.account, 0);
                                    }}
                                >
                                    Set Cost to 0
                                </gs.ButtonStyled>
                            </gs.Container>
                        ) : null }
                        <gs.SpacerSmall />
                        <gs.ButtonStyled
                            disabled={loading ? 1 : 0}
                            onClick={(e) => {
                                e.preventDefault();
                                mintNFT(blockchain.account, 1);
                            }}
                        >
                            MINT NFT
                        </gs.ButtonStyled>
                        <gs.SpacerMedium />
                        <gs.TextDescription>Balance: {data.balance}</gs.TextDescription>
                        <gs.Container jc={"center"} fd={"row"} style={{ flexWrap: "wrap" }}>
                            {data.allOwnerCExtracts.map((item, index) => {
                                return (
                                    <gs.Container key={index} style={{ padding:"15px" }}>
                                        <Renderer cext={item} />
                                        <gs.SpacerXSmall />
                                        <gs.Container>
                                            <gs.TextDescription>ID: {item}</gs.TextDescription>
                                            <gs.TextDescription>TokenURI: {data.baseURI}{item}</gs.TextDescription>
                                        </gs.Container>
                                    </gs.Container>
                                );
                            })}
                        </gs.Container>
                        <gs.SpacerMedium />
                    </gs.Container> */}
                </gs.Container>
            )}
        </gs.Screen>
    );
}

export default App;