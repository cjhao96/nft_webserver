import React from 'react';
import ReactDom from 'react-dom';
import { Provider } from "react-redux";
import reportWebVitals from './reportWebVitals';
import App from './App';
import store from "./redux/store";
import './index.css';
import "./styles/reset.css";
import "./styles/theme.css";

ReactDom.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);

reportWebVitals();