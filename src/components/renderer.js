import React from "react";
import * as gs from "../styles/globalStyles";

const Renderer = ({ cext = null, size = 200, style }) => {
    if (!cext) {
        return null;
    }

    const index = parseInt(cext) + 1;
    
    const image = require("../assets/testNFT/Images/testNFT-" + (index).toString() + ".jpg");
    const jsonData = require("../assets/testNFT/JSON/TestNFT-" + (index).toString() + ".json");
    
    const imageStyle = {
        width: "100%",
        height: "100%",
        position: "absolute",
    };

    const {
        random,
        "random float": randomFloat,
        bool,
        date,
        regEx,
        enum: _enum,
        firstname,
        lastname,
        city,
        country,
        countryCode,
        "email uses current data": emailUsesCurrentData,
        "email from expression": emailFromExpression,
        array,
        "array of objects": arrayOfObject,
        ...others
    } = jsonData;

    let name = "";
    let age = 0;

    for(var i in others)
    {
        name = i;
        age = others[i].age;
    }

    // let tokenName = "";

    // await blockchain.erc721A_MerkleTree.methods.getTokenName(cext).call().then((result) => {
    //     tokenName = result;
    // });

    return (
        <gs.Container>
            <gs.Container
                style={{
                    minWidth: size,
                    minHeight: size,
                    background: "blue",
                    position: "relative",
                    ...style,
                }}
                >
                <img alt={"image"} src={image.default} style={imageStyle} />
            </gs.Container>
            <gs.SpacerSmall />
            <gs.Container>
                <gs.TextSubTitle>Random:</gs.TextSubTitle>
                <gs.TextDescription style={{ color: "cyan" }}>&nbsp;&nbsp;{random}</gs.TextDescription>
                
                <gs.TextSubTitle>Random Float:</gs.TextSubTitle>
                <gs.TextDescription style={{ color: "cyan" }}>&nbsp;&nbsp;{randomFloat}</gs.TextDescription>
                
                <gs.TextSubTitle>Bool:</gs.TextSubTitle>
                <gs.TextDescription style={{ color: "cyan" }}>&nbsp;&nbsp;{bool.toString()}</gs.TextDescription>
                
                <gs.TextSubTitle>Date:</gs.TextSubTitle>
                <gs.TextDescription style={{ color: "cyan" }}>&nbsp;&nbsp;{date}</gs.TextDescription>

                <gs.TextSubTitle>RegEx:</gs.TextSubTitle>
                <gs.TextDescription style={{
                    color: "cyan",
                    width: "20ch",
                    whiteSpace: "nowrap",
                    overflow: "hidden",
                    textOverflow: "ellipsis"
                }}>
                    &nbsp;&nbsp;{regEx}
                </gs.TextDescription>

                <gs.TextSubTitle>Enum:</gs.TextSubTitle>
                <gs.TextDescription style={{ color: "cyan" }}>&nbsp;&nbsp;{_enum}</gs.TextDescription>

                <gs.TextSubTitle>First Name:</gs.TextSubTitle>
                <gs.TextDescription style={{ color: "cyan" }}>&nbsp;&nbsp;{firstname}</gs.TextDescription>

                <gs.TextSubTitle>Last Name:</gs.TextSubTitle>
                <gs.TextDescription style={{ color: "cyan" }}>&nbsp;&nbsp;{lastname}</gs.TextDescription>

                <gs.TextSubTitle>City:</gs.TextSubTitle>
                <gs.TextDescription style={{ color: "cyan" }}>&nbsp;&nbsp;{city}</gs.TextDescription>

                <gs.TextSubTitle>Country:</gs.TextSubTitle>
                <gs.TextDescription style={{ color: "cyan" }}>&nbsp;&nbsp;{country}</gs.TextDescription>

                <gs.TextSubTitle>Country Code:</gs.TextSubTitle>
                <gs.TextDescription style={{ color: "cyan" }}>&nbsp;&nbsp;{countryCode}</gs.TextDescription>

                <gs.TextSubTitle>Email Uses Current Data:</gs.TextSubTitle>
                <gs.TextDescription style={{ color: "cyan" }}>&nbsp;&nbsp;{emailUsesCurrentData}</gs.TextDescription>

                <gs.TextSubTitle>Email From Expression:</gs.TextSubTitle>
                <gs.TextDescription style={{ color: "cyan" }}>&nbsp;&nbsp;{emailFromExpression}</gs.TextDescription>

                <gs.TextSubTitle>Array:</gs.TextSubTitle>
                {array.map((name, index) => {
                    return (
                        <gs.Container key={index}>
                            <gs.TextDescription style={{ color: "cyan" }}>&nbsp;&nbsp;- {name}</gs.TextDescription>
                        </gs.Container>
                    );
                })}

                <gs.TextSubTitle>Array Of Object:</gs.TextSubTitle>
                {arrayOfObject.map((item, index) => {
                    return (
                        <gs.Container key={index}>
                            <gs.TextDescription style={{ color: "cyan" }}>&nbsp;&nbsp;- {item.index}: {item["index start at 5"]}</gs.TextDescription>
                        </gs.Container>
                    );
                })}

                <gs.TextSubTitle>Name:</gs.TextSubTitle>
                <gs.TextDescription style={{ color: "cyan" }}>&nbsp;&nbsp;{name}</gs.TextDescription>

                <gs.TextSubTitle>Age:</gs.TextSubTitle>
                <gs.TextDescription style={{ color: "cyan" }}>&nbsp;&nbsp;{age}</gs.TextDescription>

                {/* <gs.TextDescription>Token Name: {tokenName}</gs.TextDescription> */}
            </gs.Container>
            <gs.SpacerSmall />
        </gs.Container>
    );
};

export default Renderer;